<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <div class="box-body">
                        <?php echo form_open('certificate/mark'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input list="browsersDotThi" name="DotThi" type="text" placeholder="Đợt Thi" class="form-control" value="<?php echo set_value('DotThi'); ?>" required>
                                <datalist id="browsersDotThi">
                                    <?php foreach($listKhoa as $k){ ?>
                                        <option value="<?php echo $k['TenDotThi']; ?>"><?php echo $k['TenDotThi']; ?></option>
                                    <?php  } ?>
                                </datalist>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input list="Phong" name="Phong" type="text" placeholder="Phòng thi" class="form-control lop" value="<?php echo set_value('Phong'); ?>" autocomplete="off">
                                    <datalist id="Phong">
                                        <?php foreach($listLop as $l){ ?>
                                            <option value="<?php echo $l['TenPhongThi']; ?>"><?php echo $l['TenPhongThi']; ?></option>
                                        <?php  } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Ngày thi (dd/mm/yyyy)" class="form-control datepicker" name="NgayThi" value="<?php echo set_value('NgayThi'); ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <?php $this->load->view('includes/notice'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <?php echo form_open('certificate/updateMark', array('name' => 'MarkUpdate')); ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Đợt thi</th>
                                <th>Phòng thi</th>
                                <th>Ngày thi</th>
                                <th>Họ Đệm</th>
                                <th>Tên</th>
                                <th>Số báo danh</th>
                                <th>Điểm lý thuyết</th>
                                <th>Điểm thực hành</th>
                                <th>Điểm trung bình</th>
                                <th>Xếp loại</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($listThisinh as $a){ ?>
                                <tr>
                                    <input type="hidden" class="id" name="id[]" value="<?php echo $a['id']; ?>">
                                    <td><?php echo $a['STT'];?></td>
                                    <td><?php echo $a['DotThi'];?></td>
                                    <td><?php echo $a['Phong'];?></td>
                                    <td><?php echo $a['NgayThi'];?></td>
                                    <td><?php echo $a['HoDem'];?></td>
                                    <td><?php echo $a['Ten'];?></td>
                                    <td><?php echo $a['SoBaoDanh'];?></td>
                                    <td><input type="text" name="DiemLyThuyet[]" class="DiemLyThuyet form-control" value="<?php echo $a['DiemLyThuyet']; ?>"></td>
                                    <td><input type="text" name="DiemThucHanh[]" class="DiemThucHanh form-control" value="<?php echo $a['DiemThucHanh']; ?>"></td>
                                    <td><input type="text" name="DTB[]" class="DTB form-control" value="<?php echo $a['DTB']; ?>"></td>
                                    <td>
                                        <select name="XepLoai[]" class="XepLoai form-control">
                                            <option value="0"<?php if($a['XepLoai'] == 0) echo ' selected="selected"'; ?>>Không Đạt</option>
                                            <option value="1"<?php if($a['XepLoai'] == 1) echo ' selected="selected"'; ?>>Đạt</option>
                                        </select>
                                    </td>
                                </tr>
                            <?php  } ?>
                            </tbody>
                        </table>
                        <?php if(count($listThisinh) > 0){?>
                            <ul class="list-inline text-right">
                                <li><input type="submit" name="submit" class="btn btn-primary" value="Cập nhật"></li>
                                <li><input type="submit" name="export" class="btn btn-primary" value="Xuất excel"></li>

                            </ul>
                        <?php } ?>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>