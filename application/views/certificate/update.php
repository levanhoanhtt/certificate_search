<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            <section class="content">
                <div class="box box-default">
                    <div class="box-body">
                        <?php $this->load->view('includes/notice'); ?>
                        <?php echo form_open('certificate/update/'.$id); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="Phong" placeholder="Phòng" class="form-control" value="<?php echo $studentId['Phong']; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php $this->Mconstants->selectConstants('buoithi', "BuoiThi",$studentId['BuoiThi'] ); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Ngày thi (dd/mm/yyyy)" class="form-control datepicker" name="NgayThi" value="<?php echo $studentId['NgayThi']; ?>" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="STT" placeholder="STT" class="form-control" value="<?php echo $studentId['STT']; ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="SoBaoDanh" placeholder="SoBaoDanh" class="form-control" value="<?php echo $studentId['SoBaoDanh']; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="HoDem" placeholder="Họ đệm" class="form-control" value="<?php echo $studentId['HoDem']; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="Ten" placeholder="Tên" class="form-control" value="<?php echo $studentId['Ten']; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php $this->Mconstants->selectConstants('genders', "GioiTinh"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        <input type="text" placeholder="Ngày sinh (dd/mm/yyyy)" class="form-control datepicker" name="NgaySinh" value="<?php echo $studentId['NgaySinh']; ?>" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="NoiSinh" placeholder="Nơi sinh" class="form-control" value="<?php echo $studentId['NoiSinh']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="SoCMTND" placeholder="Số CMNT ND" class="form-control" value="<?php echo $studentId['SoCMTND']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="ChucDanh" placeholder="Chức danh" class="form-control" value="<?php echo $studentId['ChucDanh']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Đơn vị công tác" name="DonviCongtac" class="form-control" value="<?php echo $studentId['DonviCongtac']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Thành phố" name="Thanhpho" class="form-control" value="<?php echo $studentId['Thanhpho']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Quận/ Huyện/ Thị xã" name="Quan" class="form-control" value="<?php echo $studentId['Quan']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="Xa" placeholder="Xã/ Phường/ Thị trấn" class="form-control" value="<?php echo $studentId['Xa']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Lớp" name="Lop" class="form-control" value="<?php echo $studentId['Lop']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Từ ngày (dd/mm/yyyy)" class="form-control datepicker" name="TuNgay" value="<?php echo $studentId['TuNgay']; ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Đến ngày (dd/mm/yyyy)" class="form-control datepicker" name="DenNgay" value="<?php echo $studentId['DenNgay']; ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input name="DiemLyThuyet" type="text" placeholder="Điểm lý thuyết" class="form-control" value="<?php echo $studentId['DiemLyThuyet']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input name="DiemThucHanh" type="text" placeholder="Điểm thực hành" class="form-control" value="<?php echo $studentId['DiemThucHanh']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Điểm trung bình" name="DTB" class="form-control" value="<?php echo $studentId['DTB']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php $this->Mconstants->selectConstants('xeploai', "XepLoai"); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="GhiChu" placeholder="Ghi chú" class="form-control" value="<?php echo $studentId['GhiChu']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Số hiệu chứng chỉ" name="SoHieuChungChi" class="form-control" value="<?php echo $studentId['SoHieuChungChi']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Số vào sổ chứng chỉ" name="SoVaoSoChungChi" class="form-control" value="<?php echo $studentId['SoVaoSoChungChi']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Ngày cấp (dd/mm/yyyy)" class="form-control datepicker" name="NgayCap" value="<?php echo $studentId['NgayCap']; ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Ngày nhận (dd/mm/yyyy)" class="form-control datepicker" name="NgayNhan" value="<?php echo $studentId['NgayNhan']; ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="GhiChu2" placeholder="Ghi chú" class="form-control" value="<?php echo $studentId['GhiChu2']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input list="browsersDotThi" name="DotThi" type="text" placeholder="Đợt Thi" class="form-control" value="<?php echo $studentId['DotThi']; ?>" required>
                                <datalist id="browsersDotThi">
                                    <?php foreach($listKhoa as $k){ ?>
                                        <option value="<?php echo $k['TenDotThi']; ?>"><?php echo $k['TenDotThi']; ?></option>
                                    <?php  } ?>
                                </datalist>
                            </div>
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                                <div class="form-group text-right">
                                    <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <div class="modal fade" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="modalImport">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Import thí sinh</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" id="urlKhoa" value="<?php echo base_url('PhongThi/getLopByKhoa');?>">
                                <?php $this->Mconstants->selectObject($listKhoa, 'TenDotThi', 'TenDotThi', 'TenDotThi', '0', true, "Chọn đọt thi"); ?>
                            </div>
                            <div class="form-group" id="divLop">
                                <?php $this->Mconstants->selectObject($listLop, 'TenPhongThi', 'TenPhongThi', 'TenPhongThi', '0', true, "Chọn phòng thi"); ?>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="fileUrl" placeholder="Link file" disabled>
                                    <span class="input-group-btn">
                                          <button type="button" class="btn btn-info btn-flat" id="btnUpload">Upload</button>
                                    </span>
                                </div>
                            </div>
                            <img src="assets/vendor/dist/images/loading.gif" id="imgLoad" style="display: block;margin-left: auto;margin-right: auto;">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                            <button type="button" class="btn btn-primary" id="btnImport">Cập nhật</button>
                            <input type="text" id="uploadFileUrl" hidden="hidden" value="<?php echo base_url('certificate/import'); ?>">
                            <input type="text" id="markUrl" hidden="hidden" value="<?php echo base_url('certificate'); ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>