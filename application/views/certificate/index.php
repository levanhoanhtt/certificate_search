<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <?php echo form_open('certificate'); ?>
            <section class="content">
                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input list="dotthi" name="dotthi" type="text" placeholder="Đợt thi" class="form-control dotthi" value="<?php echo set_value('dotthi'); ?>" autocomplete="off">
                                    <input type="hidden" id="urlKhoa" value="<?php echo base_url('Certificate/getPhongthiByDotthi');?>">
                                    <datalist id="dotthi">
                                        <?php foreach($listKhoa as $l){ ?>
                                            <option value="<?php echo $l['TenDotThi']; ?>"><?php echo $l['TenDotThi']; ?></option>
                                        <?php  } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input list="lop" name="lop" type="text" placeholder="Phòng thi" class="form-control lop" value="<?php echo set_value('lop'); ?>" autocomplete="off">
                                    <datalist id="lop">
                                        <?php foreach($listLop as $l){ ?>
                                            <option value="<?php echo $l['TenPhongThi']; ?>"><?php echo $l['TenPhongThi']; ?></option>
                                        <?php  } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" list="browsersQuan" name="Quan" placeholder="Quận/ Huyện/ Thị xã" class="form-control" value="<?php echo set_value('Quan'); ?>" autocomplete="off">
                                    <datalist id="browsersQuan">
                                        <?php foreach($listQuan as $q){ ?>
                                            <option value="<?php echo $q['Quan']; ?>"><?php echo $q['Quan']; ?></option>
                                        <?php  } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="Xa" placeholder="Xã/ Phường/ Thị trấn" class="form-control" value="<?php echo set_value('Xa'); ?>">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Ngày thi (dd/mm/yyyy)" class="form-control datepicker" name="NgayThi" value="<?php echo set_value('NgayThi'); ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="HoDem" placeholder="Họ đệm" class="form-control" value="<?php echo set_value('HoDem'); ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="Ten" placeholder="Tên" class="form-control" value="<?php echo set_value('Ten'); ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="SoBaoDanh" placeholder="Số báo danh" class="form-control" value="<?php echo set_value('SoBaoDanh'); ?>">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Ngày sinh (dd/mm/yyyy)" class="form-control datepicker" name="NgaySinh" value="<?php echo set_value('NgaySinh'); ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                </div>
                            </div>
                        </div>
                        <!--Ho ten, tw ngay, den ngay-->
                    </div>
                </div>
            </section>
            <?php $this->load->view('includes/notice'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, '<input type="submit" name="export" class="btn btn-primary" value="Xuất Excel">'); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Ngày thi</th>
                                <th>Số báo danh</th>
                                <th style="width: 135px;">Họ đệm</th>
                                <th>Tên</th>
                                <th>Ngày Sinh</th>
                                <th>Điểm lý thuyết</th>
                                <th>Điểm thực hành</th>
                                <th>Điểm trung bình</th>
                                <th>Xếp loại</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($listThiSinh as $s){ ?>
                                <tr>
                                    <td><?php echo $s['NgayThi']; ?></td>
                                    <td><?php echo $s['SoBaoDanh']; ?></td>
                                    <td><?php echo $s['HoDem']; ?></td>
                                    <td><?php echo $s['Ten']; ?></td>
                                    <td><?php echo $s['NgaySinh']; ?></td>
                                    <td><?php echo $s['DiemLyThuyet']; ?></td>
                                    <td><?php echo $s['DiemThucHanh']; ?></td>
                                    <td><?php echo $s['DTB']; ?></td>
                                    <td><?php echo $s['XepLoai']; ?></td>
                                    <td>
                                        <a href="<?php echo base_url('certificate/update/'.$s['id']) ?>" <i class="fa fa-pencil"></i></a>
                                        <a href="<?php echo base_url('certificate/delete/'.$s['id']) ?>" onclick="return confirm('Bạn có thực sự muốn xóa?');" title="Xóa"><i class="fa fa-trash-o"></i></a></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
            <?php echo form_close(); ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>