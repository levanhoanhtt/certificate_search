<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Khóa</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyWarehouse">
                            <?php
                            foreach($listKhoa as $w){ ?>
                                <tr id="warehouse_<?php echo $w['id']; ?>">
                                    <td id="warehouseName_<?php echo $w['id']; ?>"><?php echo $w['TenKhoa']; ?></td>
                                    <td class="actions">
                                        <a href="#" class="link_edit" data-id="<?php echo $w['id']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="#" class="link_delete" data-id="<?php echo $w['id']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('khoa/update', array('id' => 'warehouseForm')); ?>
                                <td><input type="text" class="form-control hmdrequired" id="warehouseName" name="WarehouseName" value="" data-field="Thuộc tính"></td>
                                <td class="actions">
                                    <a href="#" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="#" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="WarehouseId" id="warehouseId" value="0" hidden="hidden">
                                    <input type="text" id="deleteWarehouseUrl" value="<?php echo base_url('khoa/delete'); ?>" hidden="hidden">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>