<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <?php echo form_open('student'); ?>
            <section class="content">
                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" id="khoa" list="Khoa" name="khoa" placeholder="Khóa học" class="form-control" value="<?php echo set_value('khoa'); ?>" autocomplete="off">
                                    <input type="hidden" id="urlKhoa" value="<?php echo base_url('Lop/getLopByKhoa');?>">
                                    <datalist id="Khoa">
                                        <?php foreach($listKhoa as $k){ ?>
                                            <option value="<?php echo $k['TenKhoa']; ?>"><?php echo $k['TenKhoa']; ?></option>
                                        <?php  } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input list="lop" name="lop" type="text" placeholder="Lớp" class="form-control lop" value="<?php echo set_value('lop'); ?>" autocomplete="off">
                                    <datalist id="lop">
                                        <?php foreach($listLop as $l){ ?>
                                            <option value="<?php echo $l['TenLop']; ?>"><?php echo $l['TenLop']; ?></option>
                                        <?php  } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" list="browsersQuan" name="Quan" placeholder="Quận/ Huyện/ Thị xã" class="form-control" value="<?php echo set_value('Quan'); ?>" autocomplete="off">
                                    <datalist id="browsersQuan">
                                        <?php foreach($listQuan as $q){ ?>
                                            <option value="<?php echo $q['Quan']; ?>"><?php echo $q['Quan']; ?></option>
                                        <?php  } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="Xa" placeholder="Xã/ Phường/ Thị trấn" class="form-control" value="<?php echo set_value('Xa'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="HoDem" placeholder="Họ đệm" class="form-control" value="<?php echo set_value('HoDem'); ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="Ten" placeholder="Tên" class="form-control" value="<?php echo set_value('Ten'); ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Ngày bắt đầu (dd/mm/yyyy)" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Ngày kết thúc (dd/mm/yyyy)" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Ngày sinh (dd/mm/yyyy)" class="form-control datepicker" name="NgaySinh" value="<?php echo set_value('NgaySinh'); ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            </div>
                        </div>
                        <!--Ho ten, tw ngay, den ngay-->
                    </div>
                </div>
            </section>
            <?php $this->load->view('includes/notice'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, '<input type="submit" name="export" class="btn btn-primary" value="Xuất Excel">'); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Lớp</th>
                                <th style="width: 135px;">Họ đệm</th>
                                <th>Tên</th>
                                <th>Ngày Sinh</th>
                                <th>Quận/ Huyện/ Thị xã</th>
                                <th>Xã/ Phường/ Thị trấn</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($listThiSinh as $s){ ?>
                                    <tr>
                                        <td><?php echo $s['STT']; ?></td>
                                        <td><?php echo $s['TenLop']; ?></td>
                                        <td><?php echo $s['HoDem']; ?></td>
                                        <td><?php echo $s['Ten']; ?></td>
                                        <td><?php echo $s['NgaySinh']; ?></td>
                                        <td><?php echo $s['Quan']; ?></td>
                                        <td><?php echo $s['Xa']; ?></td>
                                        <td>
                                            <a href="<?php echo base_url('student/update/'.$s['id']) ?>" <i class="fa fa-pencil"></i></a>
                                            <a href="<?php echo base_url('student/delete/'.$s['id']) ?>" onclick="return confirm('Bạn có thực sự muốn xóa?');" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>