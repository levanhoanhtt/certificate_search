<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>

            </section>
            <section class="content">
                <div class="box box-default">
                    <div class="box-body">
                        <?php $this->load->view('includes/notice'); ?>
                        <?php echo form_open('student/update/'.$studentId); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input list="browsersKhoa" name="TenKhoa" type="text" placeholder="Khóa học" class="form-control" value="<?php echo $studenId['TenKhoa']; ?>" required>
                                    <datalist id="browsersKhoa">
                                        <?php foreach($listKhoa as $k){ ?>
                                            <option value="<?php echo $k['TenKhoa']; ?>"><?php echo $k['TenKhoa']; ?></option>
                                        <?php  } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input list="browsersLop" name="TenLop" type="text" placeholder="Lớp" class="form-control" value="<?php echo $studenId['TenLop']; ?>" required>
                                    <datalist id="browsersLop">
                                        <?php foreach($listLop as $l){ ?>
                                            <option value="<?php echo $l['TenLop']; ?>"><?php echo $l['TenLop']; ?></option>
                                        <?php  } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Từ ngày (dd/mm/yyyy)" class="form-control datepicker" name="TuNgay" value="<?php echo ddMMyyyy($studenId['TuNgay']); ?>" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Đến ngày (dd/mm/yyyy)" class="form-control datepicker" name="DenNgay" value="<?php echo ddMMyyyy($studenId['DenNgay']); ?>" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="STT" placeholder="STT" class="form-control" value="<?php echo $studenId['STT']; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="HoDem" placeholder="Họ đệm" class="form-control" value="<?php echo $studenId['HoDem']; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="Ten" placeholder="Tên" class="form-control" value="<?php echo $studenId['Ten']; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php $this->Mconstants->selectConstants('genders', "GioiTinh"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        <input type="text" placeholder="Ngày sinh (dd/mm/yyyy)" class="form-control datepicker" name="NgaySinh" value="<?php echo $studenId['NgaySinh']; ?>" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="NoiSinh" placeholder="Nơi sinh" class="form-control" value="<?php echo $studenId['NoiSinh']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="SoCMTND" placeholder="Số CMNT ND" class="form-control" value="<?php echo $studenId['SoCMTND']; ?>">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="ChucDanh" placeholder="Chức danh" class="form-control" value="<?php echo $studenId['ChucDanh']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Thành phố" name="Thanhpho" class="form-control" value="<?php echo $studenId['Thanhpho']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Quận/ Huyện/ Thị xã" name="Quan" class="form-control" value="<?php echo $studenId['Quan']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" name="Xa" placeholder="Xã/ Phường/ Thị trấn" class="form-control" value="<?php echo $studenId['Xa']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Đơn vị công tác" name="DonviCongtac" class="form-control" value="<?php echo $studenId['DonviCongtac']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input name="TrinhdoHocvan" type="text" placeholder="Trình độ học vấn" class="form-control" value="<?php echo $studenId['TrinhdoHocvan']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Điện thoại" name="Dienthoai" class="form-control" value="<?php echo $studenId['Dienthoai']; ?>">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Email" name="Email" class="form-control" value="<?php echo $studenId['Email']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Ghi chú" name="GhiChu" class="form-control" value="<?php echo set_value('GhiChu'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Dân tộc" name="DanToc" class="form-control" value="<?php echo $studenId['DanToc']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Quê quán" name="QueQuan" class="form-control" value="<?php echo $studenId['QueQuan']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                                <div class="form-group text-right">
                                    <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <div class="modal fade" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="modalImport">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Import thí sinh</h4>
                        </div>
                        <div class="modal-body">
                            <Fdiv class="form-group">
                                <input type="hidden" id="urlKhoa" value="<?php echo base_url('Lop/getLopByKhoa');?>">
                                <?php $this->Mconstants->selectObject($listKhoa, 'TenKhoa', 'TenKhoa', 'TenKhoa', '0', true, "Chọn khóa"); ?>
                            </Fdiv>
                            <div class="form-group" id="divLop">
                                <?php $this->Mconstants->selectObject($listLop, 'TenLop', 'TenLop', 'TenLop', '0', true, "Chọn lớp"); ?>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="fileUrl" placeholder="Link file" disabled>
                                    <span class="input-group-btn">
                                          <button type="button" class="btn btn-info btn-flat" id="btnUpload">Upload</button>
                                    </span>
                                </div>
                            </div>
                            <img src="assets/vendor/dist/images/loading.gif" id="imgLoad" style="display: block;margin-left: auto;margin-right: auto;">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                            <button type="button" class="btn btn-primary" id="btnImport">Cập nhật</button>
                            <input type="text" id="uploadFileUrl" hidden="hidden" value="<?php echo base_url('student/import'); ?>">
                            <input type="text" id="markUrl" hidden="hidden" value="<?php echo base_url('student/mark'); ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>