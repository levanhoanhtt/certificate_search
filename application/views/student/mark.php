<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <div class="box-body">
                        <?php echo form_open('student/mark'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" id="khoa" list="Khoa" name="khoa" placeholder="Khóa học" class="form-control" value="<?php echo set_value('khoa'); ?>" autocomplete="off">
                                    <input type="hidden" id="urlKhoa" value="<?php echo base_url('Lop/getLopByKhoa');?>">
                                    <datalist id="Khoa">
                                        <?php foreach($listKhoa as $k){ ?>
                                            <option value="<?php echo $k['TenKhoa']; ?>"><?php echo $k['TenKhoa']; ?></option>
                                        <?php  } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input list="lop" name="lop" type="text" placeholder="Lớp" class="form-control lop" value="<?php echo set_value('lop'); ?>" autocomplete="off">
                                    <datalist id="lop">
                                        <?php foreach($listLop as $l){ ?>
                                            <option value="<?php echo $l['TenLop']; ?>"><?php echo $l['TenLop']; ?></option>
                                        <?php  } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Ngày bắt đầu (dd/mm/yyyy)" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" placeholder="Ngày kết thúc (dd/mm/yyyy)" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <?php $this->load->view('includes/notice'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <?php echo form_open('student/updateMark', array('name' => 'MarkUpdate')); ?>
                            <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Họ Đệm</th>
                                <th>Tên</th>
                                <th>Ngày sinh</th>
                                <th>Số buổi học</th>
                                <th>Số buổi học thực tế</th>
                                <th>Điểm lý thuyết</th>
                                <th>Điểm thực hành</th>
                                <th>Điểm trung bình</th>
                                <th>Đủ điều kiện dự thi</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($listThisinh as $a){ ?>
                                    <tr>
                                        <input type="hidden" class="id" name="id[]" value="<?php echo $a['id']; ?>">
                                        <td><?php echo $a['STT'];?></td>
                                        <td><?php echo $a['HoDem'];?></td>
                                        <td><?php echo $a['Ten'];?></td>
                                        <td><?php echo $a['NgaySinh'];?></td>
                                        <td><input type="text" name="SoBuoiHoc[]" class="SoBuoiHoc form-control" value="<?php echo $a['SoBuoiHoc']; ?>"></td>
                                        <td><input type="text" name="SoBuoiHocThucTe[]" class="SoBuoiHocThucTe form-control" value="<?php echo $a['SoBuoiHocThucTe']; ?>"></td>
                                        <td><input type="text" name="DiemLyThuyet[]" class="DiemLyThuyet form-control" value="<?php echo $a['DiemLyThuyet']; ?>"></td>
                                        <td><input type="text" name="DiemThucHanh[]" class="DiemThucHanh form-control" value="<?php echo $a['DiemThucHanh']; ?>"></td>
                                        <td><input type="text" class="DTB form-control" value="<?php echo $a['DTB'];?>" disabled></td>
                                        <td>
                                            <select name="DuThi[]" class="DuThi form-control">
                                                <option class="no" value="0"<?php if($a['DuThi'] == 0) echo ' selected="selected"'; ?>>Không</option>
                                                <option class="yes" value="1"<?php if($a['DuThi'] == 1) echo ' selected="selected"'; ?>>Có</option>
                                            </select>
                                        </td>
                                    </tr>
                                <?php  } ?>
                            </tbody>
                        </table>
                        <?php if(count($listThisinh) > 0){?>
                            <ul class="list-inline text-right">
                                <li><input type="submit" name="submit" class="btn btn-primary" value="Cập nhật"></li>
                                <li><input type="submit" name="export" class="btn btn-primary" value="Xuất excel"></li>

                            </ul>
                        <?php } ?>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>