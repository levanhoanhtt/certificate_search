<footer class="main-footer">
    <div class="container">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2016 <a href="http://hoanmuada.com">Hoàn Mưa Đá</a>.</strong> All rights reserved.
    </div>
    <!-- /.container -->
</footer>
</div>
<script src="assets/vendor/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="assets/vendor/plugins/fastclick/fastclick.js"></script>
<script src="assets/vendor/dist/js/app.min.js"></script>
<script src="assets/vendor/plugins/pnotify/pnotify.custom.js"></script>
<script src="assets/vendor/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/js/common.js"></script>
<!--<script type="text/javascript" src="assets/ckfinder/ckfinder.js"></script>-->
<?php if(isset($scriptFooter)) outputScript($scriptFooter); ?>
</body>
</html>