<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ThuPham
 * Date: 11/4/2016
 * Time: 9:12 PM
 */
class Mchungchi extends MY_Model
{
    function __construct() {
        parent::__construct();
        $this->_table_name = 'chungchi';
        $this->_primary_key = 'id';
    }

    public function getListQuan(){
        return $this->getByQuery('SELECT DISTINCT Quan from chungchi');
    }

    public function search($postData){
        $query = "SELECT * FROM chungchi WHERE 1=1";
        if(isset($postData['HoDem']) && !empty($postData['HoDem'])) $query.=" AND HoDem LIKE '%{$postData['HoDem']}%'";
        if(isset($postData['Ten']) && !empty($postData['Ten'])) $query.=" AND Ten LIKE '%{$postData['Ten']}%'";
        if(isset($postData['lop']) && !empty($postData['lop'])) $query.=" AND Phong = '{$postData['lop']}'";
        if(isset($postData['Quan']) && !empty($postData['Quan'])) $query.=" AND Quan LIKE '%{$postData['Quan']}%'";
        if(isset($postData['Xa']) && !empty($postData['Xa'])) $query.=" AND Xa LIKE '%{$postData['Xa']}%'";
        if(isset($postData['NgayThi']) && !empty($postData['NgayThi'])) $query.=" AND NgayThi='{$postData['NgayThi']}'";
        if(isset($postData['NgaySinh']) && !empty($postData['NgaySinh'])) $query.=" AND NgaySinh='{$postData['NgaySinh']}'";
        if(isset($postData['SoBaoDanh']) && !empty($postData['SoBaoDanh'])) $query.=" AND SoBaoDanh ='{$postData['SoBaoDanh']}'";
        if(isset($postData['dotthi']) && !empty($postData['dotthi'])) $query.=" AND DotThi ='{$postData['dotthi']}'";
        if(isset($postData['Phong']) && !empty($postData['Phong'])) $query.=" AND Phong ='{$postData['Phong']}'";
        //$query.=" ORDER BY BeginDate DESC";
        //$query.=" ORDER BY BeginDate DESC";
        return $this->getByQuery($query);
    }

    public function updateBarch($data){
        $this->db->update_batch('chungchi', $data, 'id');
        return true;
    }

    public function getThisinhById($id){
        $squery = "SELECT * FROM chungchi WHERE id = ".$id;
        return $this->getByQuery($squery);
    }
}