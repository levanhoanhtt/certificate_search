<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ThuPham
 * Date: 10/17/2016
 * Time: 10:27 AM
 */
class Mkhoa extends MY_Model
{
    function __construct() {
        parent::__construct();
        $this->_table_name = 'khoa';
        $this->_primary_key = 'id';
    }
    public function delete1($id){
        $this->load->model('Mlop');
        $this->db->trans_begin();
        $this->Mlop->deleteMultiple(array('KhoaId' => $id));
        $this->delete($id);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

}