<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconstants extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public $genders = array(
        'nam' => 'Nam',
        'nữ' => 'Nữ'
    );
//chức vụ
    public $chucvu = array(
        1 => 'Chủ tịch UBND',
        2 => 'Phó Chủ tịch UBND',
        3 => 'Công chức',
        4 => 'Cán bộ',
        5 => 'Chuyên viên',
        6 => 'Cán sự',
        7 => 'Viên chức',
        8 => 'Nhân viên'
    );
//chức danh
    public $chucdanh = array(
        1 => 'Văn phòng-Thống kê',
        2 => 'Địa chính-Xây dựng',
        3 => 'Phó Chủ tịch UBND',
        4 => 'Tư pháp-Hộ tịch',
        5 => 'Tài chính-Kế toán',
        6 => 'Chủ tịch UBND',
        7 => 'Văn hóa-Xã hội',
        8 => 'Chỉ huy trưởng quân sự',
        9 => 'Trưởng công an',
        10 => 'Một cửa',
        11 => 'Hiệu trưởng',
        12 => 'Phó Hiệu trưởng',
        13 => 'Giáo viên',
        14 => 'Nhân viên',
        15 => 'Viên chức'
    );
//trình độ học vấn
    public $hocvan = array(
        1 => 'Cử nhân/Đại học',
        2 => 'Cao đẳng',
        3 => 'Trung cấp',
        4 => 'Tiến sĩ',
        5 => '12/10',
        6 => 'Thạc sĩ',
        7 => 'Kỹ sư',
        8 => '10/10'
    );
//buổi thi
    public $buoithi = array(
        'Sáng' => 'Sáng',
        'Chiều' => 'Chiều',
        'Tối' => 'Tối'
    );
//xếp loại
    public $xeploai = array(
        'Giỏi' => 'Giỏi',
        'Khá' => 'Khá',
        'Trung bình' => 'Trung bình',
        'Đạt' => 'Đạt',
        'Không đạt' => 'Không đạt'
    );

    public $labelCss = array(
        1 => 'label label-default',
        2 => 'label label-success',
        3 => 'label label-warning',
        4 => 'label label-danger',
        5 => 'label label-default',
        6 => 'label label-success',
        7 => 'label label-warning',
        8 => 'label label-danger'
    );

    public function selectConstants($key, $selectName, $itemId = 0, $isAll = false, $txtAll = 'Tất cả'){
        $obj = $this->$key;
        if($obj) {
            echo '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'">';
            if($isAll) echo '<option value="0">'.$txtAll.'</option>';
            foreach($obj as $i => $v){
                if($itemId == $i) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$v.'</option>';
            }
            echo "</select>";
        }
    }

    public function selectObject($listObj, $objKey, $objValue, $selectName, $objId = 0, $isAll = false, $txtAll = "", $selectClass = '', $attrSelect = ''){
        $id = str_replace('[]', '', lcfirst($selectName));
        echo '<select class="form-control'.$selectClass.'" name="'.$selectName.'" id="'.$id.'"'.$attrSelect.'>';
        if($isAll){
            if(empty($txtAll)) echo '<option value="0">Tất cả</option>';
            else echo '<option value="0">'.$txtAll.'</option>';
        }
        $isSelectMutiple = is_array($objId);
        foreach($listObj as $obj){
            $selected = '';
            if(!$isSelectMutiple) {
                if ($obj[$objKey] == $objId) $selected = ' selected="selected"';
            }
            elseif(in_array($obj[$objKey], $objId)) $selected = ' selected="selected"';
            echo '<option value="'.$obj[$objKey].'"'.$selected.'>'.$obj[$objValue].'</option>';
        }
        echo '</select>';
    }

    public function selectNumber($start, $end, $selectName, $itemId = 0, $asc = false, $attrSelect = ''){
        echo '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'"'.$attrSelect.'>';
        if($asc){
            for($i = $start; $i <= $end; $i++){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        else{
            for($i = $end; $i >= $start; $i--){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        echo '</select>';
    }

    public function getObjectValue($listObj, $objKey, $objValue, $objKeyReturn){
        foreach($listObj as $obj){
            if($obj[$objKey] == $objValue) return $obj[$objKeyReturn];
        }
        return '';
    }
}
