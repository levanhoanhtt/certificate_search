<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ThuPham
 * Date: 10/17/2016
 * Time: 10:27 AM
 */
class Mthisinh extends MY_Model
{
    function __construct() {
        parent::__construct();
        $this->_table_name = 'thisinh';
        $this->_primary_key = 'id';
    }

    public function getListQuan(){
        return $this->getByQuery('SELECT DISTINCT Quan from thisinh');
    }

    public function search($postData){
        $query = "SELECT * FROM thisinh WHERE 1=1";
        if(isset($postData['HoDem']) && !empty($postData['HoDem'])) $query.=" AND HoDem LIKE '%{$postData['HoDem']}%'";
        if(isset($postData['Ten']) && !empty($postData['Ten'])) $query.=" AND Ten LIKE '%{$postData['Ten']}%'";
        if(isset($postData['lop']) && !empty($postData['lop'])) $query.=" AND TenLop LIKE '%{$postData['lop']}%'";
        if(isset($postData['khoa']) && !empty($postData['khoa'])) $query.=" AND TenKhoa LIKE '%{$postData['khoa']}%'";
        if(isset($postData['Quan']) && !empty($postData['Quan'])) $query.=" AND Quan LIKE '%{$postData['Quan']}%'";
        if(isset($postData['Xa']) && !empty($postData['Xa'])) $query.=" AND Xa LIKE '%{$postData['Xa']}%'";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query.=" AND (TuNgay IS NULL OR TuNgay >='{$postData['BeginDate']}')";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query.=" AND (DenNgay IS NULL OR TuNgay<='{$postData['EndDate']}')";
        if(isset($postData['NgaySinh']) && !empty($postData['NgaySinh'])) $query.=" AND (NgaySinh = '{$postData['NgaySinh']}')";
        //$query.=" ORDER BY BeginDate DESC";
        //$query.=" ORDER BY BeginDate DESC";
        return $this->getByQuery($query);
    }

    public function getThisinhByID($id){
        $squery = "SELECT * FROM thisinh WHERE id = ".$id;
        return $this->getByQuery($squery);
    }

    public function updateBarch($data){
        $this->db->update_batch('thisinh', $data, 'id');
        return true;
    }


}