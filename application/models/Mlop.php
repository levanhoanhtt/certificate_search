<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ThuPham
 * Date: 10/17/2016
 * Time: 10:25 AM
 */
class Mlop extends MY_Model
{
    function __construct() {
        parent::__construct();
        $this->_table_name = 'lop';
        $this->_primary_key = 'id';
    }

    public function delete1($id){
        $tenLop = $this->getFieldValue(array('id' => $id), 'TenLop');
        $this->load->model('Mthisinh');
        $this->db->trans_begin();
        $this->Mthisinh->deleteMultiple(array('TenLop' => $tenLop));
        $this->delete($id);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

}