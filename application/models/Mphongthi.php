<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ThuPham
 * Date: 11/4/2016
 * Time: 8:19 PM
 */
class Mphongthi extends MY_Model
{
    function __construct() {
        parent::__construct();
        $this->_table_name = 'phongthi';
        $this->_primary_key = 'id';
    }

    public function getPhongthiByDotthi($dotThi){
        return $this->getByQuery('SELECT phongthi.* FROM dotthi, phongthi WHERE phongthi.id = dotthi.id AND dotthi.TenDotThi = ? ', array('TenDotThi' => $dotThi));
    }

    public function delete1($id){
        $this->load->model('Mchungchi');
        $tenPhong = $this->getFieldValue(array('id' => $id), 'TenPhongThi');
        $this->db->trans_begin();
        $this->Mchungchi->deleteMultiple(array('Phong' => $tenPhong));
        $this->delete($id);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }
}