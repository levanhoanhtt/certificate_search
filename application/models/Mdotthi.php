<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ThuPham
 * Date: 11/4/2016
 * Time: 8:14 PM
 */
class Mdotthi extends MY_Model
{
    function __construct() {
        parent::__construct();
        $this->_table_name = 'dotthi';
        $this->_primary_key = 'id';
    }

    public function delete1($id){
        $this->load->model('Mphongthi');
        $this->load->model('Mchungchi');
        $tenDotThi = $this->getFieldValue(array('id' => $id), 'TenDotThi');
        $this->db->trans_begin();
        $this->Mphongthi->deleteMultiple(array('DotThiID' => $id));
        $this->Mchungchi->deleteMultiple(array('DotThi' => $tenDotThi));
        $this->delete($id);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }
}