<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ThuPham
 * Date: 11/4/2016
 * Time: 8:33 PM
 */
class Phongthi extends  MY_Controller
{
    public function index(){
        $this->loadModel(array('Mdotthi', 'Mphongthi'));
        $data = array(
            'title' => 'Danh sách Phòng thi',
            'listLop' => $this->Mphongthi->get(),
            'listKhoa' => $this->Mdotthi->get(),
            'scriptFooter' => array('js' => 'js/page_setting_phongthi.js')
        );
        $this->load->view('setting/phongthi', $data);
    }

    public function delete(){
        $id = $this->input->post('id');
        if($id > 0){
            $this->load->model('Mphongthi');
            $check = $this->Mphongthi->delete1($id);
        };
    }

    public function update(){
        $data = array();
        $id = $this->input->post('id');
        $data['TenPhongThi'] = trim($this->input->post('tenLop'));
        $data['DotThiID'] = $this->input->post('KhoaId');
        $this->load->model('Mphongthi');
        $id = $this->Mphongthi->save($data, $id);
        echo json_encode(array('id' => $id));
    }

    public function getLopByKhoa(){
        $TenKhoa = $this->input->post('khoa');
        $query = 'SELECT phongthi.* FROM dotthi, phongthi WHERE phongthi.DotThiID = dotthi.id AND dotthi.TenDotThi = ? ';
        $param = array(
            'TenDotThi' => $TenKhoa
        );
        $this->load->model('Mphongthi');
        $listLop = $this->Mphongthi->getByQuery($query, $param);
        echo json_encode(array('listLop'=>$listLop));
    }

}