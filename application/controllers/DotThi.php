<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ThuPham
 * Date: 11/4/2016
 * Time: 8:20 PM
 */
class Dotthi extends MY_Controller
{
    public function index(){
        $this->load->model(array('Mdotthi'));
        $listKhoa = $this->Mdotthi->get();
        $data = array(
            'title' => 'Danh sách Đợt thi',
            'listKhoa' => $listKhoa,
            'scriptFooter' => array('js' => 'js/page_setting_dotthi.js')
        );
        $this->load->view('setting/dotthi', $data);
    }

    public function delete(){
        $id = $this->input->post('id');
        if($id > 0){
            $this->load->model('Mdotthi');
            $check = $this->Mdotthi->delete1($id);
        };
    }

    public function update(){
        $data = array();
        $id = $this->input->post('id');
        $data['TenDotThi'] = $this->input->post('tenKhoa');
        $this->load->model('Mdotthi');
        $id = $this->Mdotthi->save($data, $id);
        echo json_encode(array('id' => $id));
    }
}