<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ThuPham
 * Date: 11/4/2016
 * Time: 8:57 PM
 */
class Certificate extends MY_Controller
{
    public function index(){
        $this->loadModel(array('Mdotthi', 'Mphongthi', 'Mchungchi'));
        $data = array(
            'title' => 'Tìm kiếm',
            'listLop' => $this->Mphongthi->get(),
            'listKhoa' => $this->Mdotthi->get(),
            'listQuan' => $this->Mchungchi->getListQuan(),
            'scriptFooter' => array('js' => 'js/page_certificate_search.js')
        );
        $postData = $this->arrayFromPost(array('lop', 'Quan', 'Xa', 'HoDem', 'Ten', 'NgayThi', 'SoBaoDanh','NgaySinh', 'dotthi'));

        //if(!empty($postData['NgayThi'])) $postData['NgayThi'] = ddMMyyyyToDate($postData['NgayThi']);
        $listStudents = array();
        if($this->input->post('submit')){
            $listStudents = $this->Mchungchi->search($postData);
        }
        elseif($this->input->post('export')){
            $listStudents = $this->Mchungchi->search($postData);
            $this->export($listStudents);
        }
        $data['listThiSinh'] = $listStudents;
        if($this->session->flashdata('txtSuccess')) $data['txtSuccess'] = $this->session->flashdata('txtSuccess');
        if($this->session->flashdata('txtError')) $data['txtError'] = $this->session->flashdata('txtError');
        $this->load->view('certificate/index', $data);
    }

    public function getPhongthiByDotthi(){
        $TenKhoa = $this->input->post('dotthi');
        $this->load->model('Mphongthi');
        $listLop = $this->Mphongthi->getPhongthiByDotthi($TenKhoa);
        echo json_encode(array('listLop'=>$listLop));
    }

    public function mark(){
        $postData = $this->arrayFromPost(array('DotThi', 'Phong','NgayThi'));
        if(!empty($postData['NgayThi'])) $postData['NgayThi'] = ddMMyyyyToDate($postData['NgayThi']);
        $this->loadModel(array('Mchungchi', 'Mdotthi', 'Mphongthi'));
        $listLop = $this->Mphongthi->get();
        $listKhoa = $this->Mdotthi->get();
        $listThisinh = array();
        if($this->input->post('submit')) $listThisinh = $this->Mchungchi->search($postData);
        $data = array(
            'title' => 'Điểm số',
            'listLop' => $listLop,
            'listKhoa' => $listKhoa,
            'listThisinh' => $listThisinh,
            'scriptFooter' => array('js' => array('js/page_certificate_mark.js'))
        );
        if ($this->session->flashdata('txtSuccess')) $data['txtSuccess'] = $this->session->flashdata('txtSuccess');
        $this->load->view('certificate/mark', $data);
    }

    function updateMark(){
        $this->load->model('Mchungchi');
        if($this->input->post('submit')) {
            $id = $this->input->post('id');
            $DiemLyThuyet = $this->input->post('DiemLyThuyet');
            $DiemThucHanh = $this->input->post('DiemThucHanh');
            $DTB = $this->input->post('DTB');
            $XepLoai = $this->input->post('XepLoai');
            for ($i = 0; $i < count($id); $i++) {
                $data[]= array(
                    'DiemLyThuyet' => $DiemLyThuyet[$i],
                    'DiemThucHanh' => $DiemThucHanh[$i],
                    'DTB' => ($DiemLyThuyet[$i] + $DiemThucHanh[$i]) / 2,
                    'XepLoai' => $XepLoai[$i],
                    'id' => $id[$i]
                );

            }

            $this->Mchungchi->updateBarch($data);
            $this->session->set_flashdata('txtSuccess', "Cập nhật điểm thành công");
            redirect('certificate/mark');
        }
        elseif($this->input->post('export')) {
            $postData = $this->arrayFromPost(array('BeginDate', 'EndDate'));
            if(!empty($postData['BeginDate']))$postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
            if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
            $listStudents = $this->Mchungchi->search($postData);
            $this->export($listStudents);
        }
    }

    private function export($listStudents){
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $sheet = $this->excel->getActiveSheet();
        $this->excel->getDefaultStyle()->getFont()->setName('Times New Roman')->setSize(12);//->setBold(true);
        $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $sheet->getPageSetup()->setFitToPage(true);
        $sheet->getPageSetup()->setFitToWidth(1);
        $sheet->getPageSetup()->setFitToHeight(0);
        //$sheet->setPrintGridlines(true);
        $sheet->setTitle("ChungChi");
        /*$border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );*/

        //$sheet->setCellValue('A1', "Lớp");
        //$sheet->setCellValue('B1', "Từ ngày");
        //$sheet->setCellValue('C1', "Đến ngày");

        $sheet->setCellValue('A1', "Phòng");
        $sheet->setCellValue('B1', "Buổi thi");
        $sheet->setCellValue('C1', "Ngày thi");
        $sheet->setCellValue('D1', "STT");
        $sheet->setCellValue('E1', "SBD");
        $sheet->setCellValue('F1', "Họ đệm");
        $sheet->setCellValue('G1', "Tên");
        $sheet->setCellValue('H1', "Giới tính");
        $sheet->setCellValue('I1', "Ngày sinh");
        $sheet->setCellValue('J1', "Nơi sinh");
        $sheet->setCellValue('K1', "Số CMTND");
        $sheet->setCellValue('L1', "Chức danh");
        $sheet->setCellValue('M1', "Đơn vị công tác");
        $sheet->setCellValue('N1', "Thành phố");
        $sheet->setCellValue('O1', "Quận/ Huyện/ Thị xã");

        $sheet->setCellValue('P1', "Xã/ Phường/ Thị trấn");
        $sheet->setCellValue('Q1', "Lớp");
        $sheet->setCellValue('R1', "Từ ngày");
        $sheet->setCellValue('S1', "Đến ngày");
        $sheet->setCellValue('T1', "Điểm lý thuyết");
        $sheet->setCellValue('U1', "Điểm thực hành");
        $sheet->setCellValue('V1', "Điểm trung bình");
        $sheet->setCellValue('W1', "Xếp loại");
        $sheet->setCellValue('X1', "Ghi chú");
        $sheet->setCellValue('Y1', "Số hiệu chứng chỉ");
        $sheet->setCellValue('Z1', "Số vào sổ chứng chỉ");
        $sheet->setCellValue('AA1', "Ngày cấp");
        $sheet->setCellValue('AB1', "Ngày nhận");
        $sheet->setCellValue('AC1', "Ghi chú");
        $colLeters = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z','AA', 'AB', 'AC');
        foreach($colLeters as $l) $sheet->getStyle($l.'1')->getFont()->setBold(true);
        $i = 1;

        foreach($listStudents as $s){
            $i++;
            $sheet->setCellValue('A'.$i, $s['Phong']);
            $sheet->setCellValue('B'.$i, $s['BuoiThi']);
            $sheet->setCellValue('C'.$i, $s['NgayThi']);
            $sheet->setCellValue('D'.$i, $s['STT']);
            $sheet->setCellValue('E'.$i, $s['SoBaoDanh']);
            $sheet->setCellValue('F'.$i, $s['HoDem']);
            $sheet->setCellValue('G'.$i, $s['Ten']);
            $sheet->setCellValue('H'.$i, $s['GioiTinh']);
            $sheet->setCellValue('I'.$i, $s['NgaySinh']);
            $sheet->setCellValue('J'.$i, $s['NoiSinh']);
            $sheet->setCellValue('K'.$i, $s['SoCMTND']);
            $sheet->setCellValue('L'.$i, $s['ChucDanh']);
            $sheet->setCellValue('M'.$i, $s['DonviCongtac']);
            $sheet->setCellValue('N'.$i, $s['Thanhpho']);
            $sheet->setCellValue('O'.$i, $s['Quan']);

            $sheet->setCellValue('P'.$i, $s['Xa']);
            $sheet->setCellValue('Q'.$i, $s['Lop']);
            $sheet->setCellValue('R'.$i, ddMMyyyy($s['TuNgay']));
            $sheet->setCellValue('S'.$i, ddMMyyyy($s['DenNgay']));
            $sheet->setCellValue('T'.$i, $s['DiemLyThuyet']);
            $sheet->setCellValue('U'.$i, $s['DiemThucHanh']);
            $sheet->setCellValue('V'.$i, $s['DTB']);
            $sheet->setCellValue('W'.$i, $s['XepLoai']);
            $sheet->setCellValue('X'.$i, $s['GhiChu']);
            $sheet->setCellValue('Y'.$i, $s['SoHieuChungChi']);
            $sheet->setCellValue('Z'.$i, $s['SoVaoSoChungChi']);
            $sheet->setCellValue('AA'.$i, $s['NgayCap']);
            $sheet->setCellValue('AB'.$i, $s['NgayNhan']);
            $sheet->setCellValue('AC'.$i, $s['GhiChu2']);
        }
        //$sheet->getStyle("A{$j}:J{$i}")->applyFromArray($border);
        $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(true);
        foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
        $filename = "ChungChi.xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function add(){
        $this->loadModel(array('Mchungchi', 'Mphongthi', 'Mdotthi'));
        $listLop = $this->Mphongthi->get();
        $listKhoa = $this->Mdotthi->get();
        $data = array(
            'title' => 'Thêm thí sinh theo phòng thi',
            'listLop' => $listLop,
            'listKhoa' => $listKhoa,
            'scriptFooter' => array('js' => array('ckfinder/ckfinder.js', 'js/page_certificate_add.js')),
        );
        if($this->input->post('submit')) {
            $postData = $this->arrayFromPost(array('Phong', 'BuoiThi', 'NgayThi', 'STT', 'SoBaoDanh', 'HoDem', 'Ten', 'GioiTinh', 'NgaySinh', 'NoiSinh', 'SoCMTND', 'ChucDanh', 'DonviCongtac', 'Thanhpho', 'Quan', 'Xa', 'Lop', 'TuNgay', 'DenNgay', 'DiemLyThuyet', 'DiemThucHanh', 'DTB', 'XepLoai', 'GhiChu', 'SoHieuChungChi', 'SoVaoSoChungChi', 'NgayCap', 'NgayNhan', 'GhiChu2', 'DotThi'));
            $postData['TuNgay'] = ddMMyyyyToDate($postData['TuNgay']);
            $postData['DenNgay'] = ddMMyyyyToDate($postData['DenNgay']);
            $postData['NgayThi'] = ddMMyyyyToDate($postData['NgayThi']);
            $postData['NgaySinh'] = ddMMyyyyToDate($postData['NgaySinh']);
            $postData['NgayCap'] = ddMMyyyyToDate($postData['NgayCap']);
            $postData['NgayNhan'] = ddMMyyyyToDate($postData['NgayNhan']);
            $flag = $this->Mchungchi->save($postData);
            if($flag) $data['txtSuccess'] = 'Thêm thí sinh thành công';
            else $data['txtError'] = 'Thêm thí sinh thất bại';
        }
        $this->load->view('certificate/add', $data);
    }

    public function delete($id = 0){
        if($id > 0){
            $this->load->model('Mchungchi');
            $flag = $this->Mchungchi->delete($id);
            if($flag) $this->session->set_flashdata('txtSuccess', "Xóa thí sinh thành công");
            else $this->session->set_flashdata('txtError', "Xóa thí sinh  thất bại");
        }
        redirect('certificate');
    }

    public function update($id = 0){
        $this->loadModel(array('Mdotthi', 'Mphongthi', 'Mchungchi'));
        $studentId = $this->Mchungchi->getThisinhById($id)[0];
        $data = array(
            'title' => 'Sửa thông tin',
            'listLop' => $this->Mphongthi->get(),
            'listKhoa' => $this->Mdotthi->get(),
            'listQuan' => $this->Mchungchi->getListQuan(),
            'scriptFooter' => array('js' => 'js/page_certificate_search.js'),
            'studentId' => $studentId,
            'id' => $id
        );

        if($this->input->post('submit')) {
            $postData = $this->arrayFromPost(array('Phong', 'BuoiThi', 'NgayThi', 'STT', 'SoBaoDanh', 'HoDem', 'Ten', 'GioiTinh', 'NgaySinh', 'NoiSinh', 'SoCMTND', 'ChucDanh', 'DonviCongtac', 'Thanhpho', 'Quan', 'Xa', 'Lop', 'TuNgay', 'DenNgay', 'DiemLyThuyet', 'DiemThucHanh', 'DTB', 'XepLoai', 'GhiChu', 'SoHieuChungChi', 'SoVaoSoChungChi', 'NgayCap', 'NgayNhan', 'GhiChu2', 'DotThi'));
            $flag = $this->Mchungchi->save($postData, $id);
            if($flag){
                $data['txtSuccess'] = 'Cập nhật thí sinh thành công';
                $data['studentId'] = $postData;
            }
            else $data['txtError'] = 'Cập nhật thí sinh thất bại';
        }

        $this->load->view('certificate/update', $data);
    }


    public function import(){
        $fileUrl = trim($this->input->post('FileUrl'));
        $Phong = trim($this->input->post('TenPhongThi'));
        $DotThi = trim($this->input->post('TenDotThi'));
        //xoa thi sinh theo lop va khoa
        $where = array(
            'DotThi' => $DotThi,
            'Phong' => $Phong
        );
        $this->loadModel(array('Mchungchi'));
        $this->Mchungchi->deleteBy($where);
        //...........
        if(ROOT_PATH != '/') $fileUrl = str_replace(ROOT_PATH, '', $fileUrl);
        if(!empty($fileUrl) && file_exists($fileUrl)){
            $this->load->library('excel');
            $inputFileType = PHPExcel_IOFactory::identify($fileUrl);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($fileUrl);
            $objWorksheet  = $objPHPExcel->setActiveSheetIndex(0);
            $highestRow    = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();
            $highestColumnIndex =30;// PHPExcel_Cell::columnIndexFromString($highestColumn);
            $studentData = array();
            $cols = array('Phong', 'BuoiThi', 'NgayThi', 'STT', 'SoBaoDanh', 'HoDem', 'Ten', 'GioiTinh', 'NgaySinh', 'NoiSinh', 'SoCMTND', 'ChucDanh', 'DonviCongtac', 'Thanhpho', 'Quan', 'Xa','Lop', 'TuNgay', 'DenNgay', 'DiemLyThuyet', 'DiemThucHanh', 'DTB', 'XepLoai', 'GhiChu','SoHieuChungChi', 'SoVaoSoChungChi', 'NgayCap', 'NgayNhan', 'GhiChu2','DotThi');
            $flag = true;
          //  $this->loadModel(array('Mlop', 'Mkhoa'));
           // $listLop1 = $this->Mlop->get();
           // $listKhoa1 = $this->Mkhoa->get();
          //  $listKhoa = $listLop = array();
           // foreach($listKhoa1 as $k) $listKhoa[$k['id']] = $k['TenKhoa'];
          //  foreach($listLop1 as $l) $listLop[$l['TenLop']] = $listKhoa[$l['KhoaId']];
            for ($row = 3; $row <= $highestRow; $row++) {
                for ($col = 0; $col < $highestColumnIndex; $col++) {
                    $colName = $cols[$col];
                    $value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();

                    if($colName == 'Phong'){
                        if($value != $Phong){
                            //$flag = false;
                            break;
                        }
                    }
                    if($colName == 'TuNgay' || $colName == 'DenNgay') $value = ddMMyyyyToDate($value);
                    $studentData[$row - 3][$colName] = $value;
                }
               // $studentData[$row - 2]['TenKhoa'] = $listLop[$tenLop];
            }

            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
            //echo '<pre>'.print_r($studentData, true).'</pre>';
            if($flag) {
                if (!empty($studentData)) {
                    $this->db->insert_batch('chungchi', $studentData);
                    echo json_encode(array('code' => 1, 'message' => "Import chứng chỉ thành công"));
                }
                else echo json_encode(array('code' => 0, 'message' => "File không đúng định dạng"));
            }
            else echo json_encode(array('code' => 0, 'message' => "Không chọn đúng Tên phòng thi"));
        }
        else echo json_encode(array('code' => -1, 'message' => "File không tồn tại"));
    }
}