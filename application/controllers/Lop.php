<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ThuPham
 * Date: 10/17/2016
 * Time: 10:29 AM
 */
class Lop extends MY_Controller
{
    public function index(){
        $this->loadModel(array('Mlop', 'Mkhoa'));
        $data = array(
            'title' => 'Danh sách Lớp học',
            'listLop' => $this->Mlop->get(),
            'listKhoa' => $this->Mkhoa->get(),
            'scriptFooter' => array('js' => 'js/page_setting_lop.js')
        );
        $this->load->view('setting/lop', $data);
    }

    public function delete(){
        $id = $this->input->post('id');
        if($id > 0){
            $this->load->model('Mlop');
            $this->Mlop->delete1($id);
        }
    }


    public function update(){
        $data = array();
        $id = $this->input->post('id');
        $data['TenLop'] = trim($this->input->post('tenLop'));
        $data['KhoaId'] = $this->input->post('KhoaId');
        $this->load->model('Mlop');
        $id = $this->Mlop->save($data, $id);
        echo json_encode(array('id' => $id));
    }

    public function getLopByKhoa(){
        $TenKhoa = $this->input->post('khoa');
        $query = 'SELECT lop.* FROM khoa, lop WHERE lop.KhoaId = khoa.id AND khoa.TenKhoa = ? ';
        $param = array(
            'TenKhoa' => $TenKhoa
        );
        $this->load->model('Mphongthi');
        $listLop = $this->Mphongthi->getByQuery($query, $param);
        echo json_encode(array('listLop'=>$listLop));
    }

}