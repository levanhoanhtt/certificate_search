<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ThuPham
 * Date: 10/17/2016
 * Time: 10:32 AM
 */
class Khoa extends MY_Controller
{
    public function index(){
        $this->load->model(array('Mkhoa'));
        $listKhoa = $this->Mkhoa->get();
        $data = array(
            'title' => 'Danh sách Khóa học',
            'listKhoa' => $listKhoa,
            'scriptFooter' => array('js' => 'js/page_setting_khoa.js')
        );
        $this->load->view('setting/khoa', $data);
    }

    public function delete(){
        $id = $this->input->post('id');
        if($id > 0){
            $this->load->model('Mkhoa');
            $check = $this->Mkhoa->delete1($id);
        };
    }

    public function update(){
        $data = array();
        $id = $this->input->post('id');
        $data['TenKhoa'] = $this->input->post('tenKhoa');
        $this->load->model('Mkhoa');
        $id = $this->Mkhoa->save($data, $id);
        echo json_encode(array('id' => $id));
    }
}