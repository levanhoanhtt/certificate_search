<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends MY_Controller {

	public function index(){
		$this->loadModel(array('Mlop', 'Mkhoa', 'Mthisinh'));
		$data = array(
			'title' => 'Tìm kiếm',
			'listLop' => $this->Mlop->get(),
			'listKhoa' => $this->Mkhoa->get(),
			'listQuan' => $this->Mthisinh->getListQuan(),
			'scriptFooter' => array('js' => 'js/page_student_search.js')
		);
		$postData = $this->arrayFromPost(array('lop', 'khoa', 'Quan', 'Xa', 'HoDem', 'Ten', 'BeginDate', 'EndDate', 'NgaySinh'));
		if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
		if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);

		$listStudents = array();
		if($this->input->post('submit')){
			$listStudents = $this->Mthisinh->search($postData);
		}
		elseif($this->input->post('export')){
			$listStudents = $this->Mthisinh->search($postData);
			$this->export($listStudents);
		}
		$data['listThiSinh'] = $listStudents;
		if($this->session->flashdata('txtSuccess')) $data['txtSuccess'] = $this->session->flashdata('txtSuccess');
		if($this->session->flashdata('txtError')) $data['txtError'] = $this->session->flashdata('txtError');
		$this->load->view('student/index', $data);
	}

	public function mark(){
		$postData = $this->arrayFromPost(array('khoa', 'lop', 'BeginDate', 'EndDate'));
		if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
		if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
		$this->loadModel(array('Mthisinh', 'Mlop', 'Mkhoa'));
		$listLop = $this->Mlop->get();
		$listKhoa = $this->Mkhoa->get();
		$listThisinh = array();
		if($this->input->post('submit')) $listThisinh = $this->Mthisinh->search($postData);
		$data = array(
			'title' => 'Điểm số',
			'listLop' => $listLop,
			'listKhoa' => $listKhoa,
			'listThisinh' => $listThisinh,
			'scriptFooter' => array('js' => array('js/page_student_mark.js'))
		);
		if ($this->session->flashdata('txtSuccess')) $data['txtSuccess'] = $this->session->flashdata('txtSuccess');
		$this->load->view('student/mark', $data);
	}

	function updateMark(){
		$this->load->model('Mthisinh');
		if($this->input->post('submit')) {
			$id = $this->input->post('id');
			$SoBuoiHoc = $this->input->post('SoBuoiHoc');
			$SoBuoiHocThucTe = $this->input->post('SoBuoiHocThucTe');
			$DiemLyThuyet = $this->input->post('DiemLyThuyet');
			$DiemThucHanh = $this->input->post('DiemThucHanh');
			//$DTB = $this->input->post('DTB');
			$DuThi = $this->input->post('DuThi');
			for ($i = 0; $i < count($id); $i++) {
				$data[]= array(
					'SoBuoiHoc' => $SoBuoiHoc[$i],
					'SoBuoiHocThucTe' => $SoBuoiHocThucTe[$i],
					'DiemLyThuyet' => $DiemLyThuyet[$i],
					'DiemThucHanh' => $DiemThucHanh[$i],
					'DTB' => ($DiemLyThuyet[$i] + $DiemThucHanh[$i]) / 2,
					'DuThi' => $DuThi[$i],
					'id' => $id[$i]
				);
				//$this->Mthisinh->save($data, $id[$i]);
			}
			$this->Mthisinh->updateBarch($data);
			$this->session->set_flashdata('txtSuccess', "Cập nhật điểm thành công");
			redirect('student/mark');
		}
		elseif($this->input->post('export')) {
			$postData = $this->arrayFromPost(array('khoa', 'lop', 'BeginDate', 'EndDate'));
			if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
			if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
			$listStudents = $this->Mthisinh->search($postData);
			$this->export($listStudents);
		}
	}

	private function export($listStudents){
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$sheet = $this->excel->getActiveSheet();
		$this->excel->getDefaultStyle()->getFont()->setName('Times New Roman')->setSize(12);//->setBold(true);
		$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
		$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$sheet->getPageSetup()->setFitToPage(true);
		$sheet->getPageSetup()->setFitToWidth(1);
		$sheet->getPageSetup()->setFitToHeight(0);
		//$sheet->setPrintGridlines(true);
		$sheet->setTitle("DiemThi");
		/*$border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );*/

		//$sheet->setCellValue('A1', "Lớp");
		//$sheet->setCellValue('B1', "Từ ngày");
		//$sheet->setCellValue('C1', "Đến ngày");

		$sheet->setCellValue('A1', "STT");
		$sheet->setCellValue('B1', "Họ, Đệm");
		$sheet->setCellValue('C1', "Tên");
		$sheet->setCellValue('D1', "Giới tính");
		$sheet->setCellValue('E1', "Ngày sinh");
		$sheet->setCellValue('F1', "Nơi sinh");
		$sheet->setCellValue('G1', "Số CMTND");
		$sheet->setCellValue('H1', "Chức danh");
		$sheet->setCellValue('I1', "Đơn vị công tác");
		$sheet->setCellValue('J1', "Thành phố");
		$sheet->setCellValue('K1', "Quận/ Huyện/ Thị xã");
		$sheet->setCellValue('L1', "Xã/ Phường/ Thị trấn");
		$sheet->setCellValue('M1', "Trình độ học vấn");
		$sheet->setCellValue('N1', "Điện thoại");
		$sheet->setCellValue('O1', "Email");

		$sheet->setCellValue('P1', "Lớp");
		$sheet->setCellValue('Q1', "Khóa học");
		$sheet->setCellValue('R1', "Từ ngày");
		$sheet->setCellValue('S1', "Đến ngày");
		$sheet->setCellValue('T1', "Số buổi học thực tế");
		$sheet->setCellValue('U1', "Số buổi học");
		$sheet->setCellValue('V1', "Tỉ lệ thời gian học");
		$sheet->setCellValue('W1', "Điểm bài ĐK (LT)");
		$sheet->setCellValue('X1', "Điểm bài ĐK (TH)");
		$sheet->setCellValue('Y1', "Điểm bài KT ĐK");
		$sheet->setCellValue('Z1', "Được KT");
		$colLeters = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
		foreach($colLeters as $l) $sheet->getStyle($l.'1')->getFont()->setBold(true);
		$i = 2;

		foreach($listStudents as $s){
			$i++;
			$sheet->setCellValue('A'.$i, $s['STT']);
			$sheet->setCellValue('B'.$i, $s['HoDem']);
			$sheet->setCellValue('C'.$i, $s['Ten']);
			$sheet->setCellValue('D'.$i, $s['GioiTinh']);
			$sheet->setCellValue('E'.$i, $s['NgaySinh']);
			$sheet->setCellValue('F'.$i, $s['NoiSinh']);
			$sheet->setCellValue('G'.$i, $s['SoCMTND']);
			$sheet->setCellValue('H'.$i, $s['ChucDanh']);
			$sheet->setCellValue('I'.$i, $s['DonviCongtac']);
			$sheet->setCellValue('J'.$i, $s['Thanhpho']);
			$sheet->setCellValue('K'.$i, $s['Quan']);
			$sheet->setCellValue('L'.$i, $s['Xa']);
			$sheet->setCellValue('M'.$i, $s['TrinhdoHocvan']);
			$sheet->setCellValue('N'.$i, $s['Dienthoai']);
			$sheet->setCellValue('O'.$i, $s['Email']);

			$sheet->setCellValue('P'.$i, $s['TenLop']);
			$sheet->setCellValue('Q'.$i, $s['TenKhoa']);
			$sheet->setCellValue('R'.$i, ddMMyyyy($s['TuNgay']));
			$sheet->setCellValue('S'.$i, ddMMyyyy($s['DenNgay']));
			$sheet->setCellValue('T'.$i, $s['SoBuoiHocThucTe']);
			$sheet->setCellValue('U'.$i, $s['SoBuoiHoc']);
			$sheet->setCellValue('V'.$i, ($s['SoBuoiHoc'] > 0) ? round($s['SoBuoiHocThucTe'] / $s['SoBuoiHoc'] * 100)  . '%' : '0%');
			$sheet->setCellValue('W'.$i, $s['DiemLyThuyet']);
			$sheet->setCellValue('X'.$i, $s['DiemThucHanh']);
			$sheet->setCellValue('Y'.$i, $s['DTB']);
			$sheet->setCellValue('Z'.$i, ($s['DuThi'] == 1) ? 'Có' : 'Không');
		}
		//$sheet->getStyle("A{$j}:J{$i}")->applyFromArray($border);
		$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(true);
		foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
		$filename = "DiemThi.xls";
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$objWriter->save('php://output');
	}

	public function add(){
		$this->loadModel(array('Mkhoa', 'Mlop', 'Mthisinh'));
		$listKhoa = $this->Mkhoa->get();
		$listLop = $this->Mlop->get();
		$data = array(
			'title' => 'Thêm Thí sinh',
			'scriptFooter' => array('js' => array('ckfinder/ckfinder.js', 'js/page_student_add.js')),
			'listLop' => $listLop,
			'listKhoa' => $listKhoa,
		);
		if($this->input->post('submit')) {
			$postData = $this->arrayFromPost(array('TenKhoa', 'TenLop', 'TuNgay', 'DenNgay', 'STT', 'HoDem', 'Ten', 'GioiTinh', 'NgaySinh', 'NoiSinh', 'SoCMTND', 'ChucDanh', 'DonviCongtac', 'Thanhpho', 'Quan', 'Xa', 'TrinhdoHocvan', 'Dienthoai', 'Email','GhiChu', 'DanToc', 'QueQuan'));
			$postData['TuNgay'] = ddMMyyyyToDate($postData['TuNgay']);
			$postData['DenNgay'] = ddMMyyyyToDate($postData['DenNgay']);
			$flag = $this->Mthisinh->save($postData);
			if($flag) $data['txtSuccess'] = 'Thêm thí sinh thành công';
			else $data['txtError'] = 'Thêm thí sinh thất bại';
		}
		$this->load->view('student/add', $data);
	}

	public function delete($id = 0){
		if($id > 0){
			$this->load->model('Mthisinh');
			$flag = $this->Mthisinh->delete($id);
			if($flag) $this->session->set_flashdata('txtSuccess', "Xóa thí sinh thành công");
			else $this->session->set_flashdata('txtError', "Xóa thí sinh  thất bại");
		}
		redirect('student');
	}


	public function update($id = 0){
		$this->loadModel(array('Mkhoa', 'Mlop', 'Mthisinh'));
		$listKhoa = $this->Mkhoa->get();
		$listLop = $this->Mlop->get();
		$studenId = $this->Mthisinh->getThisinhByID($id)[0];
		$data = array(
			'title' => 'Sửa thông tin Thí sinh',
			'scriptFooter' => array('js' => array('ckfinder/ckfinder.js', 'js/page_student_add.js')),
			'listLop' => $listLop,
			'listKhoa' => $listKhoa,
			'studenId' => $studenId,
			'studentId' => $id
		);

		if($this->input->post('submit')) {
			$postData = $this->arrayFromPost(array('TenKhoa', 'TenLop', 'TuNgay', 'DenNgay', 'STT', 'HoDem', 'Ten', 'GioiTinh', 'NgaySinh', 'NoiSinh', 'SoCMTND', 'ChucDanh', 'DonviCongtac', 'Thanhpho', 'Quan', 'Xa', 'TrinhdoHocvan', 'Dienthoai', 'Email','GhiChu','DanToc', 'QueQuan'));
			$postData['TuNgay'] = ddMMyyyyToDate($postData['TuNgay']);
			$postData['DenNgay'] = ddMMyyyyToDate($postData['DenNgay']);
			$flag = $this->Mthisinh->save($postData, $id);
			if($flag){
				$data['txtSuccess'] = 'Cập nhật thí sinh thành công';
				$data['studenId'] = $postData;
			}
			else $data['txtError'] = 'Cập nhật thí sinh thất bại';
		}

		$this->load->view('student/update', $data);

	}

	public function import(){
		$fileUrl = trim($this->input->post('FileUrl'));
		$tenLop = trim($this->input->post('TenLop'));
		$tenKhoa = trim($this->input->post('TenKhoa'));
		//xoa thi sinh theo lop va khoa
		$where = array(
				'TenKhoa' => $tenKhoa,
				'TenLop' => $tenLop
		);
		$this->loadModel(array('Mthisinh'));
		$this->Mthisinh->deleteBy($where);
		//...........
		if(ROOT_PATH != '/') $fileUrl = str_replace(ROOT_PATH, '', $fileUrl);
		if(!empty($fileUrl) && file_exists($fileUrl)){
			$this->load->library('excel');
			$inputFileType = PHPExcel_IOFactory::identify($fileUrl);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($fileUrl);
			$objWorksheet  = $objPHPExcel->setActiveSheetIndex(0);
			$highestRow    = $objWorksheet->getHighestRow();
			$highestColumn = $objWorksheet->getHighestColumn();
			$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
			$studentData = array();
			$cols = array('TenLop', 'TuNgay', 'DenNgay', 'STT', 'HoDem', 'Ten', 'GioiTinh', 'NgaySinh', 'NoiSinh', 'SoCMTND', 'ChucDanh', 'DonviCongtac', 'Thanhpho', 'Quan', 'Xa', 'TrinhdoHocvan', 'Dienthoai', 'Email', 'QueQuan','DanToc', 'GhiChu');
			$flag = true;
			if($highestColumnIndex == count($cols)) {
				$this->loadModel(array('Mlop', 'Mkhoa'));
				$listLop1 = $this->Mlop->get();
				$listKhoa1 = $this->Mkhoa->get();
				$listKhoa = $listLop = array();
				foreach($listKhoa1 as $k) $listKhoa[$k['id']] = $k['TenKhoa'];
				foreach($listLop1 as $l) $listLop[$l['TenLop']] = $listKhoa[$l['KhoaId']];
				for ($row = 2; $row <= $highestRow; $row++) {
					for ($col = 0; $col < $highestColumnIndex; $col++) {
						$colName = $cols[$col];
						$value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
						if($colName == 'TenLop'){
							if($value != $tenLop){
								$flag = false;
								break;
							}
						}
						if($colName == 'TuNgay' || $colName == 'DenNgay') $value = ddMMyyyyToDate($value);
						$studentData[$row - 2][$colName] = $value;
					}
					$studentData[$row - 2]['TenKhoa'] = $listLop[$tenLop];
				}
			}
			$objPHPExcel->disconnectWorksheets();
			unset($objPHPExcel);
			if($flag) {
				if (!empty($studentData)) {
					$this->db->insert_batch('thisinh', $studentData);
					echo json_encode(array('code' => 1, 'message' => "Import thí sinh thành công"));
				}
				else echo json_encode(array('code' => 0, 'message' => "File không đúng định dạng"));
			}
			else echo json_encode(array('code' => 0, 'message' => "Không chọn đúng Tên lớp"));
		}
		else echo json_encode(array('code' => -1, 'message' => "File không tồn tại"));
	}


}
