/**
 * Created by ThuPham on 10/18/2016.
 */
$(document).ready(function(){
    //xóa
    $('#tbodyWarehouse').on('click', 'a.link_delete', function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type : 'post',
                url : $('input#deleteWarehouseUrl').val(),
                data : {
                    id : id
                },
                success : function () {
                    $('#tbodyWarehouse tr#warehouse_'+id).remove();
                },
                error : function () {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });

    $('#tbodyWarehouse').on('click', 'a.link_edit', function(){
        var id = $(this).attr('data-id');
        $('input#warehouseId').val(id);
        $('input#warehouseName').val($('td#warehouseName_' + id).text());
        return false;
    });

    $('a#link_update').click(function(){
        var tenKhoa = $('#warehouseName').val();
        var id = $('#warehouseId').val();
        var url = $('#warehouseForm').attr('action');
        if(tenKhoa != null && tenKhoa != ''){
            $.ajax({
                type : 'post',
                url : url,
                data : {
                    tenKhoa : tenKhoa,
                    id : id
                },
                success : function (response) {
                    var json = $.parseJSON(response);
                    if(id > 0){
                        $('td#warehouseName_'+id).text(tenKhoa);
                    }
                    else {
                        var html = '<tr id="warehouse_' +json.id+ '">';
                        html += '<td id="warehouseName_' + json.id + '">' + tenKhoa + '</td>';
                        html += '<td class="actions">' +
                            '<a href="#" class="link_edit" data-id="' + json.id + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                            '<a href="#" class="link_delete" data-id="' + json.id + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                            '</td>';
                        html += '</tr>';
                        $('#warehouseForm').parent().before(html);
                    }
                    $('#warehouseName').val('');
                    $('#warehouseId').val('0');
                },
                error : function(){
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });

    $('a#link_cancel').click(function(){
        $('#warehouseForm').trigger("reset");
        return false;
    });

});
