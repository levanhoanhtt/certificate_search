/**
 * Created by ThuPham on 11/10/2016.
 */
$(document).ready(function(){
    $('#imgLoad').hide();
    $('#importExcel').click(function(){
        $('#modalImport').modal('show');
        return false;
    });
    $('#btnUpload').click(function(){
        var finder = new CKFinder();
        finder.resourceType = 'Files';
        finder.selectActionFunction = function(fileUrl) {
            $('input#fileUrl').val(fileUrl);
        };
        finder.popup();
        return false;
    });
    $('#btnImport').click(function(){
        var fileUrl = $('input#fileUrl').val().trim();
        var tenLop = $('select#tenPhongThi').val();
        var tenKhoa = $('select#tenDotThi').val();
        if(fileUrl != '' && tenLop != '0' && tenKhoa != '0'){
            $('#imgLoad').show();
            $.ajax({
                type : 'post',
                url : $('input#uploadFileUrl').val(),
                data : {
                    TenPhongThi: tenLop,
                    TenDotThi : tenKhoa,
                    FileUrl: fileUrl
                },
                success : function (response) {
                    //console.log(response);
                    $('#imgLoad').hide();
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) redirect(false, $('#markUrl').val());
                },
                error : function () {
                    $('#imgLoad').hide();
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else showNotification('Phải chọn file excel, chọn khóa và chọn lớp để import', 0);
        return false;
    });

    $('#tenDotThi').change(function(){
        var khoa = $(this).val();
        $.ajax( {
            type: 'post',
            url : $('#urlKhoa').val(),
            data : {
                khoa : khoa
            },
            success: function(response){
                $('#tenPhongThi option').remove();
                var json = $.parseJSON(response);
                var listLop = json.listLop;
                var html = '<option value="0">Chọn phòng thi</option>';
                for(var i=0; i<listLop.length;i++){
                    html  += '<option value="'+listLop[i].TenPhongThi+'">'+listLop[i].TenPhongThi+'</option>';
                }
                $('#tenPhongThi').html(html);
            },
            error : function(){
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
});

