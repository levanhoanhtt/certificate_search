/**
 * Created by ThuPham on 10/25/2016.
 */
$(document).ready(function(){
    $('#khoa').change(function(){
        var khoa = $(this).val();
        $.ajax( {
            type: 'post',
            url : $('#urlKhoa').val(),
            data : {
                khoa : khoa
            },
            success: function(response){
                $('#lop').remove();
                var json = $.parseJSON(response);
                var listLop = json.listLop;
                var html = '<datalist id="lop">';
                for(var i=0; i<listLop.length;i++){
                    html += '<option value="'+listLop[i].TenLop+'">'+listLop[i].TenLop+'</option>';
                }
                html += '</datalist>';
                $('form .lop').after(html);
            },
            error : function(){
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
});
