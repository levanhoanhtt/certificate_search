/**
 * Created by ThuPham on 10/19/2016.
 */

$(document).ready(function(){
    $('table tbody .DiemLyThuyet, table tbody .DiemThucHanh, table tbody .SoBuoiHoc, table tbody .SoBuoiHocThucTe' ).keyup(function(){
        var  DiemLyThuyet = parseFloat($(this).parent().parent().find('td .DiemLyThuyet').val());
        var  DiemThucHanh = parseFloat($(this).parent().parent().find('td .DiemThucHanh').val());
        var SBH = parseFloat($(this).parent().parent().find('td .SoBuoiHoc').val());
        var SBHTT = parseFloat($(this).parent().parent().find('td .SoBuoiHocThucTe').val());

        var DTB = (DiemLyThuyet + DiemThucHanh)/2;
        $(this).parent().parent().find('td .DTB').val(DTB);

        var SBHTB = 0;
        if(SBH>0) {
            SBHTB = SBHTT/SBH;
            if(DTB>=5 && SBHTB>=0.9){
                $(this).parent().parent().find('td .DuThi option:selected').removeAttr('selected');
                $(this).parent().parent().find('td .DuThi .yes').attr('selected', 'selected');
            }
            else {
                $(this).parent().parent().find('td .DuThi option:selected').removeAttr('selected');
                $(this).parent().parent().find('td .DuThi .no').attr('selected', 'selected');
            }
        }

    });


    $('#khoa').change(function(){
       var khoa = $(this).val();
        $.ajax( {
           type: 'post',
            url : $('#urlKhoa').val(),
            data : {
                khoa : khoa
            },
            success: function(response){
                $('#lop').remove();
                var json = $.parseJSON(response);
                var listLop = json.listLop;
                var html = '<datalist id="lop">';
                for(var i=0; i<listLop.length;i++){
                    html += '<option value="'+listLop[i].TenLop+'">'+listLop[i].TenLop+'</option>';
                }
                html += '</datalist>';
                $('form .lop').after(html);
            },
            error : function(){
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
});
