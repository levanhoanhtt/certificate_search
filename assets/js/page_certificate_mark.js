/**
 * Created by ThuPham on 11/4/2016.
 */
$(document).ready(function(){
    $('table tbody .DiemLyThuyet, table tbody .DiemThucHanh').keyup(function(){
        var  DiemLyThuyet = parseFloat($(this).parent().parent().find('td .DiemLyThuyet').val());
        var  DiemThucHanh = parseFloat($(this).parent().parent().find('td .DiemThucHanh').val());
        var DTB = (DiemLyThuyet + DiemThucHanh)/2;
        $(this).parent().parent().find('td .DTB').val(DTB);
    });

    $('#khoa').change(function(){
        var khoa = $(this).val();
        $.ajax( {
            type: 'post',
            url : $('#urlKhoa').val(),
            data : {
                khoa : khoa
            },
            success: function(response){
                $('#lop').remove();
                var json = $.parseJSON(response);
                var listLop = json.listLop;
                console.log(listLop);
                var html = '<datalist id="lop">';
                for(var i=0; i<listLop.length;i++){
                    html += '<option value="'+listLop[i].TenPhongThi+'">'+listLop[i].TenPhongThi+'</option>';
                }
                html += '</datalist>';
                $('form .lop').after(html);
            },
            error : function(){
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
});
