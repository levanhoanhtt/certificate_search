$(document).ready(function() {
    if($(window).width() > 760) $('div.divTable').removeClass('table-responsive');
    else $('div.divTable').addClass('table-responsive');
    $(window).resize(function() {
        if($(window).width() > 760) $('div.divTable').removeClass('table-responsive');
        else $('div.divTable').addClass('table-responsive');
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
});
/* type = 1 - success
other - error
*/
function showNotification(msg, type){
    var typeText = 'error';
    if(type == 1) typeText = 'success';
    var notice = new PNotify({
        title: 'Notification',
        text: msg,
        type: typeText,
        delay: 2000,
        addclass: 'stack-bottomright',
        stack: {"dir1": "up", "dir2": "left", "firstpos1": 15, "firstpos2": 15}
    });
}

function redirect(reload, url){
    if(reload){
        window.setTimeout(function () {
            window.location.reload(true);
        }, 2000);
    }
    else{
        window.setTimeout(function() {
            window.location.href = url;
        }, 2000);
    }
}

//pagging
function pagging(pageId){
    $('input#pageId').val(pageId);
    $('input#submit').trigger('click');
}