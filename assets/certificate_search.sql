-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 24, 2017 at 12:13 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `certificate_search`
--

-- --------------------------------------------------------

--
-- Table structure for table `chungchi`
--

CREATE TABLE `chungchi` (
  `id` int(11) NOT NULL,
  `Phong` varchar(255) NOT NULL,
  `BuoiThi` varchar(255) NOT NULL,
  `NgayThi` datetime NOT NULL,
  `STT` varchar(255) NOT NULL,
  `SoBaoDanh` varchar(255) NOT NULL,
  `HoDem` varchar(255) NOT NULL,
  `Ten` varchar(255) NOT NULL,
  `GioiTinh` varchar(255) NOT NULL,
  `NgaySinh` varchar(255) NOT NULL,
  `NoiSinh` varchar(255) NOT NULL,
  `SoCMTND` varchar(255) NOT NULL,
  `ChucDanh` varchar(255) NOT NULL,
  `DonviCongtac` varchar(255) NOT NULL,
  `Thanhpho` varchar(255) NOT NULL,
  `Quan` varchar(255) NOT NULL,
  `Xa` varchar(255) NOT NULL,
  `Lop` varchar(255) NOT NULL,
  `TuNgay` datetime NOT NULL,
  `DenNgay` datetime NOT NULL,
  `DiemLyThuyet` decimal(3,1) NOT NULL,
  `DiemThucHanh` decimal(3,1) NOT NULL,
  `DTB` decimal(3,1) NOT NULL,
  `XepLoai` varchar(255) NOT NULL,
  `GhiChu` varchar(255) NOT NULL,
  `SoHieuChungChi` varchar(255) NOT NULL,
  `SoVaoSoChungChi` varchar(255) NOT NULL,
  `NgayCap` varchar(255) NOT NULL,
  `NgayNhan` varchar(255) NOT NULL,
  `GhiChu2` varchar(255) NOT NULL,
  `DotThi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chungchi`
--

INSERT INTO `chungchi` (`id`, `Phong`, `BuoiThi`, `NgayThi`, `STT`, `SoBaoDanh`, `HoDem`, `Ten`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `SoCMTND`, `ChucDanh`, `DonviCongtac`, `Thanhpho`, `Quan`, `Xa`, `Lop`, `TuNgay`, `DenNgay`, `DiemLyThuyet`, `DiemThucHanh`, `DTB`, `XepLoai`, `GhiChu`, `SoHieuChungChi`, `SoVaoSoChungChi`, `NgayCap`, `NgayNhan`, `GhiChu2`, `DotThi`) VALUES
(2, '1', 'Chiều', '0001-11-30 00:00:00', '1', '12', 'Mai Thu', 'Nga', 'nữ', '23/06/1907', '10', '1', '1', '1', '1', '1', '1', '', '2016-11-01 00:00:00', '2016-11-01 00:00:00', '6.0', '1.0', '3.5', 'Không đạt', '1', '1', '10', '2016-11-01', '2016-11-01', '', '0'),
(4, '1', 'Chiều', '0000-00-00 00:00:00', '2', '12', 'Nguyễn Thị', 'Huyền', 'nữ', '01/06/2017', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', 'Không đạt', '', '', '', '', '', '', 'Đợt 4'),
(65, '1', 'Sáng', '0000-00-00 00:00:00', '1', 'TL2-001', 'Nguyễn Thị Phúc', 'Bắc', 'Nam', '02/09/1971', 'Hà Nội', '111906673', 'Văn phòng-Thống kê', '', 'Hà Nội', 'Thạch Thất', 'Xã Bình Yên', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(66, '1', 'Sáng', '0000-00-00 00:00:00', '2', 'TL2-002', 'Nguyễn Trọng', 'Bảo', 'Nam', '30/08/1988', 'Hà Nội', '001088003130', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Thạch Thất', 'Xã Phú Kim', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(67, '1', 'Sáng', '0000-00-00 00:00:00', '3', 'TL2-003', 'Nguyễn Huy', 'Bích', 'Nam', '21/08/1967', 'Hà Nội', '001067001748', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Thạch Thất', 'Xã Dị Nậu', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(68, '1', 'Sáng', '0000-00-00 00:00:00', '4', 'TL2-004', 'Trần Văn', 'Bút', 'Nam', '20/10/1960', 'Hà Nội', '111333758', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Thạch Thất', 'Xã Tân Xã', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(69, '1', 'Sáng', '0000-00-00 00:00:00', '5', 'TL2-005', 'Nguyễn Văn', 'Chắt', 'Nam', '17/01/1958', 'Hà Nội', '011166384', 'Tài chính-Kế toán', '', 'Hà Nội', 'Thạch Thất', 'Xã Canh Nậu', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(70, '1', 'Sáng', '0000-00-00 00:00:00', '6', 'TL2-006', 'Hoàng Văn', 'Châu', 'Nam', '25/01/1978', 'Hà Nội', '111995390', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Thạch Thất', 'Xã Đồng Trúc', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(71, '1', 'Sáng', '0000-00-00 00:00:00', '7', 'TL2-007', 'Nguyễn Trung', 'Chi', 'Nam', '06/07/1981', 'Hà nội', '001081002200', 'Chủ tịch UBND', '', 'Hà Nội', 'Thạch Thất', 'Xã Canh Nậu', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(72, '1', 'Sáng', '0000-00-00 00:00:00', '8', 'TL2-008', 'Kiều Văn', 'Chiến', 'Nam', '25/09/1965', 'Hà Nội', '112415018', 'Địa chính-Xây dựng', '', 'Hà Nội', 'Thạch Thất', 'Xã Cần Kiệm', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(73, '1', 'Sáng', '0000-00-00 00:00:00', '9', 'TL2-009', 'Nguyễn Văn', 'Chiến', 'Nam', '21/07/1960', 'Hà Nội', '001060006831', 'Trưởng công an', '', 'Hà Nội', 'Thạch Thất', 'Xã Hạ Bằng', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(74, '1', 'Sáng', '0000-00-00 00:00:00', '10', 'TL2-010', 'Nguyễn Xuân', 'Chiến', 'Nam', '06/09/1969', 'Hà Nội', '111333743', 'Chỉ huy Trưởng quân sự', '', 'Hà Nội', 'Thạch Thất', 'Xã Tân Xã', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(75, '1', 'Sáng', '0000-00-00 00:00:00', '11', 'TL2-011', 'Nguyễn Duy', 'Chinh', 'Nam', '15/12/1974', 'Hà Nội', '111392494', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Thạch Thất', 'Xã Thạch Xá', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(76, '1', 'Sáng', '0000-00-00 00:00:00', '12', 'TL2-012', 'Trần Văn', 'Chung', 'Nam', '28/09/1974', 'Hà Nội', '001074008197', 'Văn hóa-Xã hội', '', 'Hà Nội', 'Thạch Thất', 'Xã Bình Yên', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(77, '1', 'Sáng', '0000-00-00 00:00:00', '13', 'TL2-013', 'Đỗ Minh', 'Chuyền', 'Nam', '10/05/1967', 'Hà Nội', '011414351', 'Chỉ huy Trưởng quân sự', '', 'Hà Nội', 'Thạch Thất', 'Xã Phùng Xá', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(78, '1', 'Sáng', '0000-00-00 00:00:00', '14', 'TL2-014', 'Vũ Thị', 'Cúc', 'Nam', '11/06/1984', 'Hà Nội', '111944178', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Thạch Thất', 'Xã Cẩm Yên', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(79, '1', 'Sáng', '0000-00-00 00:00:00', '15', 'TL2-015', 'Nguyễn Văn', 'Diên', 'Nam', '27/08/1973', 'Hà nội', '011703592', 'Phó Chủ tịch UBND', '', 'Hà Nội', 'Thạch Thất', 'Xã Bình Phú', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(80, '1', 'Sáng', '0000-00-00 00:00:00', '16', 'TL2-016', 'Nguyễn Văn', 'Điệp', 'Nam', '25/10/1976', 'Hà Nội', '001076007912', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Thạch Thất', 'Xã Canh Nậu', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(81, '1', 'Sáng', '0000-00-00 00:00:00', '17', 'TL2-017', 'Lê Quý', 'Đĩnh', 'Nam', '26/06/1966', 'Hà Nội', '111292957', 'Địa chính-Xây dựng', '', 'Hà Nội', 'Thạch Thất', 'Xã Kim Quan', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(82, '1', 'Sáng', '0000-00-00 00:00:00', '18', 'TL2-018', 'Cường Mạnh', 'Đỏ', 'Nam', '31/12/1965', 'Hà nội', '111219359', 'Phó Chủ tịch UBND', '', 'Hà Nội', 'Thạch Thất', 'Xã Thạch Xá', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(83, '1', 'Sáng', '0000-00-00 00:00:00', '19', 'TL2-019', 'Nguyễn Việt', 'Đức', 'Nam', '26/01/1987', 'Hà Nội', '001087003465', 'Địa chính-Xây dựng', '', 'Hà Nội', 'Thạch Thất', 'Xã Hạ Bằng', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(84, '1', 'Sáng', '0000-00-00 00:00:00', '20', 'TL2-020', 'Nguyễn Tiến', 'Dũng', 'Nữ', '27/05/1978', 'Hà Nội', '001078005536', 'Văn hóa-Xã hội', '', 'Hà Nội', 'Thạch Thất', 'Xã Chàng Sơn', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(85, '1', 'Sáng', '0000-00-00 00:00:00', '21', 'TL2-021', 'Nguyễn Văn', 'Dũng', 'Nữ', '23/03/1979', 'Hưng Yên', '111408822', 'Văn phòng-Thống kê', '', 'Hà Nội', 'Thạch Thất', 'Xã Thạch Hòa', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(86, '1', 'Sáng', '0000-00-00 00:00:00', '22', 'TL2-022', 'Cấn Xuân', 'Đường', 'Nữ', '12/12/1987', 'Hà Nội', '001087013159', 'Chỉ huy Trưởng quân sự', '', 'Hà Nội', 'Thạch Thất', 'Xã Cần Kiệm', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(87, '1', 'Sáng', '0000-00-00 00:00:00', '23', 'TL2-023', 'Kiều Thị', 'Duyên', 'Nữ', '01/12/1972', 'Hà Nội', '011525606', 'Phó Hiệu trưởng', '', 'Hà Nội', 'Thạch Thất', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(88, '1', 'Sáng', '0000-00-00 00:00:00', '24', 'TL2-024', 'Nguyễn Đức', 'Giạng', 'Nữ', '12/02/1959', 'Hà Nội', '111520529', 'Văn phòng-Thống kê', '', 'Hà Nội', 'Thạch Thất', 'Xã Canh Nậu', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(89, '1', 'Sáng', '0000-00-00 00:00:00', '25', 'TL2-025', 'Nguyễn Hữu', 'Hà', 'Nữ', '10/08/1963', 'Hà Nội', '001063007541', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Thạch Thất', 'Xã Hữu Bằng', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(90, '1', 'Sáng', '0000-00-00 00:00:00', '26', 'TL2-026', 'Nguyễn Thanh', 'Hải', 'Nữ', '24/09/1965', 'Hà Nội', '111333651', 'Địa chính-Xây dựng', '', 'Hà Nội', 'Thạch Thất', 'Xã Phú Kim', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(91, '1', 'Sáng', '0000-00-00 00:00:00', '27', 'TL2-027', 'Trần Văn', 'Hải', 'Nữ', '12/12/1986', 'Hà Nội', '111922159', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Thạch Thất', 'Xã Yên Trung', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(92, '1', 'Sáng', '0000-00-00 00:00:00', '28', 'TL2-028', 'Phan Thị', 'Hằng', 'Nữ', '15/03/1988', 'Hà Nội', '112164380', 'Văn hóa-Xã hội', '', 'Hà Nội', 'Thạch Thất', 'Xã Hữu Bằng', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(93, '1', 'Sáng', '0000-00-00 00:00:00', '29', 'TL2-029', 'Nguyễn Thanh', 'Hằng', 'Nữ', '03/12/1983', 'Hà Nội', '111756055', 'Văn phòng-Thống kê', '', 'Hà Nội', 'Thạch Thất', 'Xã Dị Nậu', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1'),
(94, '1', 'Sáng', '0000-00-00 00:00:00', '30', 'TL2-030', 'Nguyễn Thị', 'Hạnh', 'Nữ', '14/02/1989', 'Hà Nội', '112293204', 'Văn phòng-Thống kê', '', 'Hà Nội', 'Thạch Thất', 'Xã Bình Phú', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.0', '0.0', '0.0', '', '', '', '', '', '', '', 'Đợt 1');

-- --------------------------------------------------------

--
-- Table structure for table `dotthi`
--

CREATE TABLE `dotthi` (
  `id` int(11) NOT NULL,
  `TenDotThi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dotthi`
--

INSERT INTO `dotthi` (`id`, `TenDotThi`) VALUES
(1, 'Đợt 1'),
(2, 'Đợt 2'),
(3, 'Đợt 3'),
(4, 'Đợt 4');

-- --------------------------------------------------------

--
-- Table structure for table `khoa`
--

CREATE TABLE `khoa` (
  `id` int(11) NOT NULL,
  `TenKhoa` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `khoa`
--

INSERT INTO `khoa` (`id`, `TenKhoa`) VALUES
(1, 'D10'),
(2, 'D11'),
(3, '1');

-- --------------------------------------------------------

--
-- Table structure for table `lop`
--

CREATE TABLE `lop` (
  `id` int(11) NOT NULL,
  `TenLop` varchar(255) NOT NULL,
  `KhoaId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lop`
--

INSERT INTO `lop` (`id`, `TenLop`, `KhoaId`) VALUES
(1, 'TH01', 1),
(2, 'TH02', 1),
(3, 'TH03', 2),
(4, 'TH04', 2);

-- --------------------------------------------------------

--
-- Table structure for table `phongthi`
--

CREATE TABLE `phongthi` (
  `id` int(11) NOT NULL,
  `TenPhongThi` varchar(255) NOT NULL,
  `DotThiID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phongthi`
--

INSERT INTO `phongthi` (`id`, `TenPhongThi`, `DotThiID`) VALUES
(1, '1', 1),
(2, '104', 1),
(3, '105', 1),
(4, '103', 2),
(5, '103', 1);

-- --------------------------------------------------------

--
-- Table structure for table `thisinh`
--

CREATE TABLE `thisinh` (
  `id` int(11) NOT NULL,
  `TenKhoa` varchar(255) NOT NULL,
  `TenLop` varchar(255) NOT NULL,
  `TuNgay` datetime NOT NULL,
  `DenNgay` datetime NOT NULL,
  `STT` varchar(255) NOT NULL,
  `HoDem` varchar(255) NOT NULL,
  `Ten` varchar(255) NOT NULL,
  `GioiTinh` varchar(255) NOT NULL,
  `NgaySinh` varchar(255) NOT NULL,
  `NoiSinh` varchar(255) NOT NULL,
  `SoCMTND` varchar(255) NOT NULL,
  `ChucDanh` varchar(255) NOT NULL,
  `DonviCongtac` varchar(255) NOT NULL,
  `Thanhpho` varchar(255) NOT NULL,
  `Quan` varchar(255) NOT NULL,
  `Xa` varchar(255) NOT NULL,
  `TrinhdoHocvan` varchar(255) NOT NULL,
  `Dienthoai` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `SoBuoiHoc` smallint(6) NOT NULL,
  `SoBuoiHocThucTe` smallint(6) NOT NULL,
  `DiemLyThuyet` decimal(3,1) NOT NULL,
  `DiemThucHanh` decimal(3,1) NOT NULL,
  `DTB` decimal(3,1) NOT NULL,
  `DuThi` tinyint(4) NOT NULL,
  `QueQuan` varchar(220) NOT NULL,
  `DanToc` varchar(220) NOT NULL,
  `GhiChu` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `thisinh`
--

INSERT INTO `thisinh` (`id`, `TenKhoa`, `TenLop`, `TuNgay`, `DenNgay`, `STT`, `HoDem`, `Ten`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `SoCMTND`, `ChucDanh`, `DonviCongtac`, `Thanhpho`, `Quan`, `Xa`, `TrinhdoHocvan`, `Dienthoai`, `Email`, `SoBuoiHoc`, `SoBuoiHocThucTe`, `DiemLyThuyet`, `DiemThucHanh`, `DTB`, `DuThi`, `QueQuan`, `DanToc`, `GhiChu`) VALUES
(491, '1', 'TH01', '2017-05-31 00:00:00', '2017-06-27 00:00:00', '1', 'Nguyễn Thị Hải 1', 'Huyền', 'nữ', '29/05/2017', '', '', '', '', '', '', '', '', '', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(494, 'D10', 'TH03', '2017-06-13 00:00:00', '2017-06-28 00:00:00', '1', 'anh', 'Huyền', 'nữ', '07/06/2017', '', '', '', '', '', '', '', '', '', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(495, 'D10', 'TH03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', 'anh', 'Huyền', 'nữ', '07/06/2017', '', '', '', '', '', '', '', '', '', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(498, '1', 'TH01', '2017-06-01 00:00:00', '2017-06-30 00:00:00', '1', 'Phạm Thị', 'Thu', 'nữ', '31/05/2017', 'Nam Định', '', '', '', '', '', '', '', '', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(741, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '1', 'Nguyễn Thị Thu', 'Huyền', 'nữ', '17/01/1988', 'Hà Nội', '012508224', 'Văn phòng-Thống kê', '', 'Hà Nội', 'Quận Ba Đình', 'Phường Thành Công', '', '0978.819.396', '', 0, 0, '0.0', '0.0', '0.0', 0, '1', '2', '3'),
(742, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '2', 'Bùi Thị', 'Yến', 'nữ', '20/09/1982', 'Bắc Giang', '013136751', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Mễ Trì', '', '0965.025.281', '', 0, 0, '0.0', '0.0', '0.0', 0, '4', '5', '6'),
(743, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '3', 'Nguyễn Hữu', 'Tuấn', 'nam', '19/07/1987', 'Hà Nội', '', 'Chỉ huy trưởng quân sự', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Mễ Trì', '', '0982.063.436', '', 0, 0, '0.0', '0.0', '0.0', 0, '7', '8', '9'),
(744, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '4', 'Nguyễn Thị Hải', 'Yến', 'nữ', '07/11/1977', 'Hà Nội', '012016239', 'Tài chính-Kế toán', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Mỹ Đình 2', '', '0947.337.032', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(745, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '5', 'Đỗ Tuấn', 'Anh', 'nam', '08/10/1979', 'Hà Nội', '012023353', 'Văn hóa-Xã hội', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Mễ Trì', '', '0912.201.581', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(746, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '6', 'Nguyễn Văn', 'Tuấn', 'nữ', '28/06/1979', 'Hà Nội', '001079001154', 'Địa chính-Xây dựng', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Mỹ Đình 2', '', '0985.838.298', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(747, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '7', 'Nguyễn Đức', 'Thịnh', 'nữ', '06/08/1991', 'Hà Nội', '', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Quận Ba Đình', 'Phường Ngọc Hà', '', '0934.229.279', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(748, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '8', 'Đỗ Thị', 'Thi', 'nam', '16/08/1976', 'Hà Nội', '001176004555', 'Địa chính-Xây dựng', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Đại Mỗ', '', '0165.327.1919', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(749, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '9', 'Lê Thị Hồng', 'Vân', 'nữ', '19/03/1982', 'Hà Nội', '', 'Tài chính-Kế toán', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Mễ Trì', '', '0968.195.252', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(750, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '10', 'Ngô Chí', 'Thành', 'nữ', '24/07/1983', 'Hà Nội', '', 'Văn phòng-Thống kê', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Mễ Trì', '', '0936.080.902', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(751, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '11', 'Nghiêm Thị', 'Thu', 'nữ', '08/05/1984', 'Hà Nội', '001184011628', 'Địa chính-Xây dựng', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Đại Mỗ', '', '0989.824.373', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(752, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '12', 'Lê Thị', 'Lệ', 'nữ', '28/01/1974', 'Hà Nội', '001174004964', 'Văn hóa-Xã hội', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Đại Mỗ', '', '0912.699.571', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(753, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '13', 'Nguyễn Thị Phương', 'May', 'nữ', '02/12/1982', 'Hà Nội', '012300525', 'Tài chính-Kế toán', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Tây Mỗ', '', '0974.221.802', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(754, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '14', 'Phạm Vân', 'Khánh', 'nam', '28/09/1976', 'Hà Nội', '001176004234', 'Một cửa', '', 'Hà Nội', 'Quận Ba Đình', 'Phường Kim Mã', '', '0988.524.590', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(755, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '15', 'Nguyễn Thị', 'Thanh', 'nam', '25/12/1970', 'Hà Nội', '011801877', 'Văn hóa-Xã hội', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Đại Mỗ', '', '0912.787.687', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(756, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '16', 'Đỗ Thị Trinh', 'Nữ', 'nữ', '16/02/1980', 'Nam Định', '013135908', 'Văn phòng-Thống kê', '', 'Hà Nội', 'Quận Hoàn Kiếm', 'Phường Hàng Buồm', '', '0989.172.625', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(757, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '17', 'Ngô Thị', 'Ngọc', 'nam', '27/02/1984', 'Hà Nội', '012305690', 'Tài chính-Kế toán', '', 'Hà Nội', 'Quận Nam Từ Liêm', 'Phường Phú Đô', '', '0982.656.283', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(758, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '18', 'Kiều', 'Trang', 'nam', '22/07/1990', 'Hà Nội', '001190001474', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Quận Ba Đình', 'Phường Nguyễn Trung Trực', '', '0904.537.586', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(759, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '19', 'Đỗ Thị Hồng', 'Nhung', 'nữ', '04/10/1975', 'Hà Nội', '001175001218', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Quận Ba Đình', 'Phường Phúc Xá', '', '0904.050.367', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(760, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '20', 'Vũ Thị Kim', 'Dung', 'nam', '28/06/1963', 'Hà Nội', '011081224', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Quận Hoàn Kiếm', 'Phường Phúc Tân', '', '0988.303.446', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(761, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '21', 'Vũ Nhật', 'Anh', 'nam', '19/12/1991', 'Hà Nội', '012783886', 'Tư pháp-Hộ tịch', '', 'Hà Nội', 'Quận Hoàn Kiếm', 'Phường Hàng Bông', '', '0904.236.410', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', ''),
(762, 'D11', 'TH03', '2016-01-15 00:00:00', '2016-02-05 00:00:00', '22', 'Nguyễn Huy', 'Khôi', 'nam', '21/11/1978', 'Hà Nội', '011937116', 'Văn phòng-Thống kê', '', 'Hà Nội', 'Quận Hoàn Kiếm', 'Phường Hàng Bài', '', '', '', 0, 0, '0.0', '0.0', '0.0', 0, '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chungchi`
--
ALTER TABLE `chungchi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dotthi`
--
ALTER TABLE `dotthi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `khoa`
--
ALTER TABLE `khoa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lop`
--
ALTER TABLE `lop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phongthi`
--
ALTER TABLE `phongthi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thisinh`
--
ALTER TABLE `thisinh`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chungchi`
--
ALTER TABLE `chungchi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `dotthi`
--
ALTER TABLE `dotthi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `khoa`
--
ALTER TABLE `khoa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lop`
--
ALTER TABLE `lop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `phongthi`
--
ALTER TABLE `phongthi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `thisinh`
--
ALTER TABLE `thisinh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=763;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
